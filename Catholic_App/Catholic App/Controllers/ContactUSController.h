//
//  ContactUSController.h
//  Catholic App
//
//  Created by Ronak on 22/06/15.
//  Copyright (c) 2015 ELITE INFOWORLD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface ContactUSController : UIViewController<UIGestureRecognizerDelegate,UIAlertViewDelegate,MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate,UINavigationControllerDelegate>
{

    IBOutlet UITableView *tblView;
    IBOutlet UIView *Viewaddress;
    IBOutlet UIView *viewheadernotification;
    NSString *strdevicename;


}
- (IBAction)btnsettingClicked:(id)sender;
- (IBAction)btnPushNotification:(id)sender;
- (IBAction)btnmenuClicked:(id)sender;

@end
