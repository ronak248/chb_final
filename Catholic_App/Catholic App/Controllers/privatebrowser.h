//
//  privatebrowser.h
//  Ishto
//
//  Created by Ronak on 15/04/15.
//  Copyright (c) 2015 elite infoworld. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TOWebViewController.h"
@interface privatebrowser : UIViewController<UIActionSheetDelegate>
{
    IBOutlet UILabel *lblTopBar;
    IBOutlet UIWebView *wbView;
    TOWebViewController *webViewController;
    IBOutlet UIScrollView *scrview;;
    UIRefreshControl *objrefeshcnt;
    IBOutlet UILabel *lblprayer;
    
    
}

@property(nonatomic,strong)NSURL *strweblink;
@property(nonatomic,strong)NSString *strttitle;

- (IBAction)btnmenuClicked:(id)sender;

-(IBAction)btnbackClicked:(id)sender;
-(IBAction)btnOPtionlinkCLicked:(id)sender;

@end
