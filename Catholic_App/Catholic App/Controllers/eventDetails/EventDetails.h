//
//  EventDetails.h
//  Catholic HUB
//
//  Created by Ronak on 06/09/15.
//  Copyright (c) 2015 ELITE INFOWORLD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"

@interface EventDetails : UIViewController<iCarouselDataSource, iCarouselDelegate>
{

    IBOutlet UIPageControl *pagecontrol;

    NSMutableArray *arrayevents;

}

@property (nonatomic, strong) IBOutlet iCarousel *carousel;
- (IBAction)btnMenuClicked:(id)sender;

@end
