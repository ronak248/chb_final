//
//  EventDetails.m
//  Catholic HUB
//
//  Created by Ronak on 06/09/15.
//  Copyright (c) 2015 ELITE INFOWORLD. All rights reserved.
//

#import "EventDetails.h"

@interface EventDetails ()

@end

@implementation EventDetails

- (void)viewDidLoad {
    
    _carousel.type = iCarouselTypeLinear;
    _carousel.pagingEnabled=true;
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated
{

   
    [Appdelegate ShowAnimatedActivityInd];
    PFQuery *query= [PFQuery queryWithClassName:@"EventFeed"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [Appdelegate HideAnimatedActivityInd];
                
            });
            arrayevents=[[NSMutableArray alloc]init];
            arrayevents=(NSMutableArray *)objects;
            
            NSArray* reversedArray = [[arrayevents reverseObjectEnumerator] allObjects];
            
            arrayevents=(NSMutableArray*)reversedArray;
            
            pagecontrol.numberOfPages=[arrayevents count];

            [_carousel reloadData];
            
        }
        
        }];


}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -
#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    

    NSInteger value=[arrayevents count];
    return value;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    
    
    UILabel *label = nil;
    UIImageView *imgbg = nil;
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        
        
        //        if ([[[arrayusers objectAtIndex:index] valueForKey:@"username"]isEqualToString:[objappdelegate.dicProfiledata valueForKey:@"username"]]) {
        //
        //            view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 105, 105)];
        //            imgbg = [[PFImageView alloc] initWithFrame:CGRectMake(0, 0, 105, 105)];
        //            label = [[UILabel alloc] initWithFrame:CGRectMake(0, view.bounds.size.height, 105, 30)];
        //
        //        }
        //        else
        //
        //        {
        
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width, _carousel.frame
                                                             .size.height)];
        imgbg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, _carousel.frame
                                                              .size.width, _carousel.frame
                                                              .size.height)];
        
        
        //}
        
        [imgbg setCenter:view.center];
        
        [label setTextColor:[UIColor darkGrayColor]];
        
        //((PFImageView *)view).image = [UIImage imageNamed:@"page.png"];
        
      
        [imgbg setContentMode:UIViewContentModeScaleAspectFit];
        imgbg.clipsToBounds=YES;
        
        
        PFFile *thumbnail = [[arrayevents objectAtIndex:index] objectForKey:@"Eimage"];
        NSString *url=[thumbnail url];
        
        
        [imgbg setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"trackplaceholder-1.png"]];
        //imgbg
       // imgbg.file = thumbnail;
        
       // [imgbg setContentMode:UIViewContentModeScaleAspectFit];
        //[imgbg loadInBackground];
        //view.contentMode = UIViewContentModeCenter;
        label.backgroundColor = [UIColor clearColor];
        [label setTextAlignment:NSTextAlignmentCenter];
        [label setFont:[UIFont fontWithName:@"Helvetica Neue-Bold" size:15.0]];
        label.tag = 1;
        
        [view addSubview:imgbg];
    }
    else
    {
        //get a reference to the label in the recycled view
        label = (UILabel *)[view viewWithTag:1];
    }
    
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
    
    return view;
}



- (CGFloat)carousel:(iCarousel *)_carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    int a= (int)[_carousel currentItemIndex];
    
    pagecontrol.currentPage=a;
    
    if(option == iCarouselOptionVisibleItems)
        
        return [arrayevents count];

    
    if(option == iCarouselOptionWrap) return YES;
    return value;
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index;
{
    
    NSString *strurl=[[arrayevents objectAtIndex:index] objectForKey:@"eWebsite"];


    NSURL *myURL;
    if ([strurl.lowercaseString hasPrefix:@"http://"]) {
        myURL = [NSURL URLWithString:strurl];
    } else {
        myURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",strurl]];
    }
    

    [[UIApplication sharedApplication] openURL:myURL];


}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnMenuClicked:(id)sender {
    
    [self presentLeftMenuViewController:nil];

}
@end
