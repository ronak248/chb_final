//
//  SettingController.m
//  Catholic HUB
//
//  Created by Ronak on 10/09/15.
//  Copyright (c) 2015 ELITE INFOWORLD. All rights reserved.
//

#import "SettingController.h"
#import "EventFeedPostController.h"
#import "StatusPostController.h"
@interface SettingController ()

@end

@implementation SettingController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
   
    return 2;
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *CellIdentifier = @"CellUser";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    
    
    if (indexPath.row==0) {
        
        cell.textLabel.text=[NSString stringWithFormat:@"Special Events"];
    }
    else if (indexPath.row==1) {
        
        
        cell.textLabel.text=[NSString stringWithFormat:@"Status Feed"];
    }

    
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    if (indexPath.row==0) {
        
        
        EventFeedPostController *objpost=[Appdelegate.storyevent instantiateViewControllerWithIdentifier:@"EventFeedPostController"];
        
        [self.navigationController pushViewController:objpost animated:YES];
        
    }
    else if (indexPath.row==1) {
        StatusPostController *objpost=[Appdelegate.storystfeed instantiateViewControllerWithIdentifier:@"StatusPostController"];
        [self.navigationController pushViewController:objpost animated:YES];
    }
    
}
- (IBAction)btnbackClicked:(id)sender
{



    [self.navigationController popViewControllerAnimated:YES];

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
