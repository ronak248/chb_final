//
//  StatusPostController.m
//  Catholic HUB
//
//  Created by Ronak on 10/09/15.
//  Copyright (c) 2015 ELITE INFOWORLD. All rights reserved.
//

#import "StatusPostController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "IQKeyboardManager.h"
@interface StatusPostController ()

@end

@implementation StatusPostController

- (void)viewDidLoad {
    
    
    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.mediaTypes =[UIImagePickerController availableMediaTypesForSourceType:imagePicker.sourceType];
    imagePicker.allowsEditing=NO;
    
    imagePicker.delegate = self;
    type=1;
    viewPicker.frame=CGRectMake(0, [UIScreen mainScreen].bounds.size.height-250, [UIScreen mainScreen].bounds.size.width, 300);

    txtpost.text=@"What's on your mind?";
    txtpost.textColor=[UIColor lightGrayColor];
    [txtpost becomeFirstResponder];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{


    [[IQKeyboardManager sharedManager] disableToolbarInViewControllerClass:[self class]];



}

-(void)viewWillDisappear:(BOOL)animated
{


    [[IQKeyboardManager sharedManager] setEnable:true];



}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



- (void)textViewDidEndEditing:(UITextView *)textView
{
    
    if ([textView.text isEqualToString:@""])
    {
        
        textView.textColor=[UIColor lightGrayColor];
        
        textView.text=@"What's on your mind?";
        
    }
    
}

- (IBAction)btnDateClicked:(id)sender {
    
    [self.view endEditing:YES];
    
    if (viewPicker)
    {
        [viewPicker removeFromSuperview];
    }
    
    [self.view addSubview:viewPicker];
}

- (IBAction)btnTimeClicked:(id)sender {
}

- (IBAction)btnImageClicked:(id)sender {
    
    
    [self.view endEditing:YES];

    UIActionSheet *actionsheetBlockUser=[[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo",@"Photo Library", nil];
    [actionsheetBlockUser showInView:self.view];
}

- (IBAction)btncamraClicked:(id)sender {
        
    type=1;

    [txtpost resignFirstResponder];
    UIActionSheet *actionsheetBlockUser=[[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo",@"Photo Library", nil];
    [actionsheetBlockUser showInView:self.view];
    
    [viewEvent removeFromSuperview];
    
}
- (IBAction)btnPickerCancelClicked:(id)sender
{
    [viewPicker removeFromSuperview];

}
- (IBAction)btnPickerDoneClicked:(id)sender
{
    NSDateFormatter *datefromatter=[[NSDateFormatter alloc]init];
    [datefromatter setDateFormat:@"YYYY-MM-dd hh:mm:ss a"];
    
    NSString *strdate=[datefromatter stringFromDate:[datepicker date]];
    strcurrentdate=strdate;
    NSArray *array=[strdate componentsSeparatedByString:@" "];
    
    
    txtdate.text=[array objectAtIndex:0];
    txttime.text=[array objectAtIndex:1];

    [viewPicker removeFromSuperview];

}
- (IBAction)btnLinkClicked:(id)sender {
    
    type=2;
    [txtpost becomeFirstResponder];

    [viewEvent removeFromSuperview];

}

- (IBAction)btnEventClicked:(id)sender {
    
    type=3;

    viewEvent.frame=CGRectMake(0, 65, viewEvent.frame.size.width, viewEvent.frame.size.height);
    [self.view addSubview:viewEvent];
}

- (IBAction)btnCancelClicked:(id)sender
{
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btnPostClicked:(id)sender
{

    PFObject *recipe;

    if (type==3) {
        recipe = [PFObject objectWithClassName:@"EventBookFeed"];
        [recipe setObject:txtTitle.text forKey:@"Title"];
        [recipe setObject:strcurrentdate forKey:@"SelectDate"];
        [recipe setObject:txtlocation.text forKey:@"Location"];
        [recipe setObject:txtlink.text forKey:@"Link"];
        [recipe setObject:@"3" forKey:@"type"];
        [recipe setObject:@"0" forKey:@"like"];
        [recipe setObject:@"0" forKey:@"comment"];





        
        //2
        
        NSData *imageData = UIImageJPEGRepresentation(imgcamera, 0.8);
        NSString *filename = [NSString stringWithFormat:@"user.png"];
        PFFile *imageFile = [PFFile fileWithName:filename data:imageData];
        [recipe setObject:imageFile forKey:@"Eimage"];
        
    }
    
    else if (type==2) {
        recipe = [PFObject objectWithClassName:@"EventBookFeed"];
        [recipe setObject:txtpost.text forKey:@"Status"];
        [recipe setObject:@"2" forKey:@"type"];
        [recipe setObject:@"0" forKey:@"like"];
        [recipe setObject:@"0" forKey:@"comment"];
        
        
        
    }
    else if (type==1) {
        recipe = [PFObject objectWithClassName:@"EventBookFeed"];
        [recipe setObject:txtpost.text forKey:@"Status"];
        [recipe setObject:@"1" forKey:@"type"];
        [recipe setObject:@"0" forKey:@"like"];
        [recipe setObject:@"0" forKey:@"comment"];
        
        NSData *imageData = UIImageJPEGRepresentation(imgcamera, 0.8);
        NSString *filename = [NSString stringWithFormat:@"user.png"];
        PFFile *imageFile = [PFFile fileWithName:filename data:imageData];
        [recipe setObject:imageFile forKey:@"Eimage"];
        
        
        
    }
    
        [Appdelegate ShowAnimatedActivityInd];
        
        //3
        [recipe saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [Appdelegate HideAnimatedActivityInd];
                
            });
            if (error){
                
                NSString *errorString = [[error userInfo] objectForKey:@"error"];
                UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:@"Error" message:errorString delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [errorAlertView show];
                
                
                
                //...handle errors
            }
            else
            {
                
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Successfully added status" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
                [self.navigationController popViewControllerAnimated:YES];
                
                
                
            }
        }];
        
        
        


}
#pragma---------------------
#pragma mark ActionSheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    if(buttonIndex==0)
    {
        
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            imagePicker.sourceType  = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:imagePicker animated:YES completion:nil];
            
        }
        else
        {
            
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
            [self presentViewController:imagePicker animated:NO completion:nil];
        }
    }
    else if(buttonIndex==1)
    {
        
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:imagePicker animated:NO completion:nil];
        
    }
    
    
    
}

#pragma---------------------
#pragma mark imagepicker Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image=[info valueForKey:UIImagePickerControllerOriginalImage];
    imgcamera=image;
    
    imgcamera=[Appdelegate imageWithImage:image scaledToSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.width)];
    if (type!=3) {
        type=1;

    }
    else
        txtselctimg.text=@"Selected image";
    if (type==1) {
        
        imgBg.image=image;
        imgBg.hidden=false;
        self.wbiew.hidden=true;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    //        if (isUploadingCoverPic)
    //        {
    //            [self postJsonWithStep:2];
    //        }
    //        else
    //        {
    //}
    
}

#define MAX_LENGTH 200 // Whatever your limit is
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    
    textView.autocapitalizationType=UITextAutocapitalizationTypeWords;
    if ([textView.text isEqualToString:@"What's on your mind?"])
    {
        textView.text=@"";
        textView.textColor=[UIColor lightGrayColor];
    }
    else if([textView.text length]==0)
    {
        
        
    }
    
    textView.textColor=[UIColor blackColor];
    
    NSString *youTubeToken=[Appdelegate youtubelinkget:textView.text];
    
    if (youTubeToken!=nil) {
        self.wbiew.scalesPageToFit=NO;
        type=2;

        imgBg.hidden=true;
        self.wbiew.hidden=false;
        float width = 295;
        float height = 150;
        // NSString *youTubeToken = @"k4ixAfJ1LuI";
        
        NSMutableString *html = [NSMutableString string];
        [html appendString:@"<html>"];
        [html appendString:@"<head>"];
        [html appendString:@"<style type=\"text/css\">"];
        [html appendString:@"body {"];
        [html appendString:@"background-color: transparent;"];
        [html appendString:@"color: white;"];
        [html appendString:@"margin: 0;"];
        [html appendString:@"}"];
        [html appendString:@"</style>"];
        [html appendString:@"</head>"];
        [html appendString:@"<body>"];
        [html appendFormat:@"<iframe id=\"ytplayer\" type=\"text/html\" width=\"%0.0f\" height=\"%0.0f\" src=\"http://www.youtube.com/embed/%@\" frameborder=\"0\"/>", width, height, youTubeToken];
        [html appendString:@"</body>"];
        [html appendString:@"</html>"];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                if (Isloaded==FALSE) {
                    [self.wbiew loadHTMLString:html baseURL:nil];
                    Isloaded=TRUE;
                }
                
                CGSize sizeDesc = [txtpost sizeThatFits:CGSizeMake(300, FLT_MAX)];
                self.wbiew.frame=CGRectMake(10, sizeDesc.height+90, 300, 150);
                self.wbiew.hidden=FALSE;
                //[tblView scrollToRowAtIndexPath:pathForCenterCell atScrollPosition:UITableViewScrollPositionTop animated:YES];
                
            });
        });
    }
    else if(isloaddata==false)
    {
        if ([Appdelegate detectlink:textView.text]) {
            
        
            type=2;

        youTubeToken=@"";
        imgBg.hidden=true;
        self.wbiew.hidden=false;
        NSURL *url = [Appdelegate detectlink:textView.text];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        [self.wbiew loadRequest:requestObj];
        self.wbiew.scalesPageToFit=YES;
        self.wbiew.hidden=FALSE;
        }
    }
    NSUInteger newLength = (textView.text.length - range.length) + text.length;
    if (newLength==0)
    {
        self.lblcounter.text=[NSString stringWithFormat:@"%d",200];
        
    }
    else
    {
        int length=(int)MAX_LENGTH-newLength;
        if(newLength <= MAX_LENGTH)
        {
            self.lblcounter.text=[NSString stringWithFormat:@"%d",length];
            
            return YES;
        }
        else
        {
            NSUInteger emptySpace = MAX_LENGTH - (textView.text.length - range.length);
            textView.text = [[[textView.text substringToIndex:range.location]
                              stringByAppendingString:[text substringToIndex:emptySpace]]
                             stringByAppendingString:[textView.text substringFromIndex:(range.location + range.length)]];
            return NO;
        }
    }
    return YES;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    if (action == @selector(paste:))
    {
        return NO;
    }
    else if (action == @selector(copy:))
    {
        return NO;
    }
    
    return [super canPerformAction:action withSender:sender];
}

@end
