//
//  StatusPostController.h
//  Catholic HUB
//
//  Created by Ronak on 10/09/15.
//  Copyright (c) 2015 ELITE INFOWORLD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StatusPostController : UIViewController<UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>
{

    IBOutlet UITextView *txtpost;
    UIImagePickerController *imagePicker;
    UIImage *imgcamera;
    BOOL Isloaded;

    IBOutlet UIView *viewEvent;
    IBOutlet UITextField *txtlocation;
    IBOutlet UITextField *txttime;
    IBOutlet UITextField *txtdate;
    IBOutlet UITextField *txtTitle;
    IBOutlet UITextField *txtselctimg;

    IBOutlet UITextView *txtdescription;

    
    IBOutlet UIImageView *imgBg;
    IBOutlet UIView *viewPicker;
    IBOutlet UIView *viewdatetime;
    int type;
    NSString *strcurrentdate;
    BOOL isloaddata;
    IBOutlet UITextField *txtlink;
    IBOutlet UIDatePicker *datepicker;

}
@property (nonatomic, retain)  IBOutlet UILabel *lblcounter;
@property (nonatomic, retain)  IBOutlet UIWebView *wbiew;

- (IBAction)btnDateClicked:(id)sender;
- (IBAction)btnTimeClicked:(id)sender;
- (IBAction)btnImageClicked:(id)sender;
- (IBAction)btncamraClicked:(id)sender;
- (IBAction)btnLinkClicked:(id)sender;
- (IBAction)btnEventClicked:(id)sender;
- (IBAction)btnCancelClicked:(id)sender;
- (IBAction)btnPickerCancelClicked:(id)sender;
- (IBAction)btnPickerDoneClicked:(id)sender;
- (IBAction)btnPostClicked:(id)sender;


@end
