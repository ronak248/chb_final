//
//  notificationlist.h
//  Ishto
//
//  Created by Tejas Ardeshna on 3/2/15.
//  Copyright (c) 2015 elite infoworld. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "chatRoomCell.h"


@interface notificationlist : UIViewController
{
    
    IBOutlet UISearchBar *objSearchBar;
    IBOutlet UITableView *tblView;
    NSMutableArray *arrProduct;
    NSMutableArray *arrUserList;
    int pageHashTag;
    UIRefreshControl *objRef;

    int pageTitle;
    int pageUser;
    NSMutableArray *arrayhashtagresult;
        UILabel *lblNoData;
    int totalHashTag;
    IBOutlet UITextField *txtSearch;
    IBOutlet UIView *viewheader;
    IBOutlet UILabel *lblvalues;
    IBOutlet UISlider *sliderproximaty;
    int totalTitle;
    int totalUser;
}

-(IBAction)backButtonClicked:(id)sender;
-(IBAction)btnsearchClicked:(id)sender;
-(IBAction)btnSliderchangeClicked:(id)sender;

- (IBAction)btnmenuClicked:(id)sender;


@end

