//
//  PaymentVC.h
//  Catholic App
//
//  Created by Ronak on 22/06/15.
//  Copyright (c) 2015 ELITE INFOWORLD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

#import "TOWebViewController.h"

@interface PaymentVC : UIViewController<UISplitViewControllerDelegate, UIPopoverControllerDelegate,UIGestureRecognizerDelegate>
{
    
    TOWebViewController* webViewController;
    IBOutlet UIView *viewheader1;
    IBOutlet UITableView *tblView;
    IBOutlet UIView*viewheader;
    IBOutlet UITextField *txtAmount;
    
    IBOutlet UIScrollView *scrView;
    
    int rowpayment;
    int rowprograme;
   int  rowselectionfor;
    
    IBOutlet UIWebView *wvPreview;

    IBOutlet UIView *viewlbl;
    
    IBOutlet UIButton *btnonline;
    IBOutlet UIView *viewheader0;


}




@property(nonatomic, strong, readwrite) NSString *environment;
@property(nonatomic, assign, readwrite) BOOL acceptCreditCards;
@property(nonatomic, strong, readwrite) NSString *resultText;
-(IBAction)btnproceedCLicked:(id)sender;
-(IBAction)btnbackClicked:(id)sender;
- (IBAction)btnCallclciked:(id)sender;
- (IBAction)btnonlineClicked:(id)sender;
- (IBAction)btnmenuClicked:(id)sender;

- (IBAction)btnsponserClicked:(id)sender;

@end
