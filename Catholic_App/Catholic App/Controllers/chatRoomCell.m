//
//  chatRoomCell.m
//  ishto
//
//  Created by Tejas Ardeshna on 1/29/15.
//  Copyright (c) 2015 elite infoworld. All rights reserved.
//

#import "chatRoomCell.h"

@implementation chatRoomCell

- (void)awakeFromNib {
    // Initialization code
}
-(void)Cell_Reload_Input_Views
{
    [self setNeedsDisplay];
    for (UIView *V in [self subviews])
    {
        [V reloadInputViews];
        [V setNeedsLayout];
        [V setNeedsDisplay];
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
