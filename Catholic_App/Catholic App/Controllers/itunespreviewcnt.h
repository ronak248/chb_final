//
//  itunespreviewcnt.h
//  Flotrack
//
//  Created by Ronak on 03/10/13.
//  Copyright (c) 2013 ELITE INFOWORLD. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface itunespreviewcnt : UIViewController<UITabBarControllerDelegate,UIGestureRecognizerDelegate>
{

   
    ALTabBarController *tabBarController;
    NSMutableArray *arrayHomescreens;
    IBOutlet UICollectionView *collTblview;
    NSMutableArray *arrayImages;
    IBOutlet UIButton *btngrid;
    IBOutlet UIButton *btnlist;
    UIRefreshControl *refreshControl;

    IBOutlet UIButton *btnsearchtrack;
    IBOutlet UIButton *btnsrcicons;
    IBOutlet UIButton *btnserachstatus;
    IBOutlet UIButton *btntitleartist;
    IBOutlet UIButton *btnhashtags;
    
    IBOutlet UITableView *tblView;
    IBOutlet UIView *viewpoup;
    IBOutlet UITextField *txtSearchbar;
    IBOutlet UISearchBar *objsearchitunes;
    NSString *strUserSearchKeyword;
    int searchtype;
    int searchsubtype;
 
    int totalrecords;
    NSString *strdevoctoken;
    
    
    NSMutableArray *arraytrackresult;
    NSMutableArray *arrayhashtagresult;
      NSMutableArray *arraycheck;
    
    NSString *stritunesSearchTerm;
    

}
@property(nonatomic,retain)NSString *stritunesSearchTerm;
@property(nonatomic,retain)NSString *strTypecloud;
@property BOOL isTrack;
@property (nonatomic, retain) IBOutlet ALTabBarController *tabBarController;

- (IBAction)bttnMenuClikced:(id)sender;

- (IBAction)btnRefreshClikced:(id)sender;

- (IBAction)btnGridClicked:(id)sender;
-(IBAction)btnlistClicked:(id)sender;
-(IBAction)reloAdbuttonClicked:(id)sender;
-(void)reloAdbuttonClicked;
-(IBAction)btnSearchtrack:(id)sender;
-(IBAction)btnsearchicon:(id)sender;
-(IBAction)btnsearchstatus:(id)sender;
-(IBAction)btntitleartist:(id)sender;
-(IBAction)btnhashtags:(id)sender;
-(IBAction)backButtonclicked:(id)sender;
@end
