//
//  pushnotification.m
//  Catholic App
//
//  Created by Ronak on 02/07/15.
//  Copyright (c) 2015 ELITE INFOWORLD. All rights reserved.
//

#import "pushnotification.h"
#import <MessageUI/MessageUI.h>
#import <Parse/Parse.h>
@interface pushnotification ()

@end

@implementation pushnotification

- (void)viewDidLoad {
    
    
    txtdescription.text=@"";
    txtemail.text=@"";
    txtname.text=@"";
    txtphone.text=@"";
    txtdescription.textColor=[UIColor lightGrayColor];
    txtdescription.layer.borderWidth=1.0f;
    txtdescription.layer.borderColor=[UIColor lightGrayColor].CGColor;
    [txtdescription.layer setCornerRadius:2.0f];
    txtdescription.layer.masksToBounds=true;
    
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(resignKeyboard)];
    tap.delegate=self;
    tap.cancelsTouchesInView=false;
    [self.view addGestureRecognizer:tap];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)resignKeyboard
{
    [self.view endEditing:YES];
    //[scrollView setContentOffset:CGPointZero animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btnbackClicked:(id)sender
{



    [self.navigationController popViewControllerAnimated:YES];

}

-(IBAction)btnsubmitCLicked:(id)sender
{


 
    if(txtname.text.length==0 || [txtname.text isEqualToString:@""])
    {
        
        [Appdelegate showAlertWithTitle:@"" andMessge:@"please enter title"];
        
    }
    
     else if(txtdescription.text.length==0 || [txtdescription.text isEqualToString:@""]||[txtdescription.text isEqualToString:@"Description"])
    {
        
        [Appdelegate showAlertWithTitle:@"" andMessge:@"please enter description"];
        
    }
    else
    {
        
        
     
        
        PFObject *recipe = [PFObject objectWithClassName:@"notification"];
        [recipe setObject:txtdescription.text forKey:@"notificationtext"];
        [recipe setObject:txtname.text forKey:@"title"];
        [recipe setObject:@"1" forKey:@"read"];

     
        
        // Show progress
        [Appdelegate ShowAnimatedActivityInd];
        
        // Upload recipe to Parse
        [recipe saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            [Appdelegate HideAnimatedActivityInd];
            
            if (!error) {
                
               // Show success message
               UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Successfully send notification" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
               [alert show];
                
                PFQuery *pushQuery = [PFInstallation query];
                [pushQuery whereKey:@"deviceType" equalTo:@"ios"];
                NSString* str=txtname.text;
                
                str = [str substringToIndex: MIN(50, [txtname.text length])];
                
                // Send push notification to query
                [PFPush sendPushMessageToQueryInBackground:pushQuery
                                               withMessage:str];
                
                [pushQuery whereKey:@"deviceType" equalTo:@"android"];
                [PFPush sendPushMessageToQueryInBackground:pushQuery
                                               withMessage:str];
                
                txtdescription.text=@"";
                txtname.text=@"";

               
               
               // Dismiss the controller
                
            } else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Upload Failure" message:[error localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
            }
            
        }];
        
        
        
    }

}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result)
    {
        case MessageComposeResultCancelled:
        {
            //NSLog(@"Cancelled");
            
        }
            
            break;
        case MessageComposeResultFailed:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Fail" message:@"Message Fail to send"
                                                           delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
            break;
            
        case MessageComposeResultSent:
            //NSLog(@"Message Sent Successfully");
            break;
        default:
            break;
    }
    [self dismissViewControllerAnimated:NO completion:nil];
    
}
- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent)
    {
        //NSLog(@"It's away!");
    }
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([textView.text isEqualToString:@"Post here..."])
    {
        textView.text=@"";
        textView.textColor=[UIColor lightGrayColor];
    }
    else if([textView.text length]==0)
    {
        
        
    }
    else
    textView.textColor=[UIColor blackColor];
    

        return YES;
   
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    
    if ([textView.text isEqualToString:@""])
    {
        
        textView.textColor=[UIColor lightGrayColor];
        
        textView.text=@"Post here...";
        
    }
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [self.view endEditing:YES];

    return true;

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
