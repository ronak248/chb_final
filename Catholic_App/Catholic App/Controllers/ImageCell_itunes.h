//
//  ImageCell_itunes.h
//  Flotrack
//
//  Created by Ronak on 04/10/13.
//  Copyright (c) 2013 ELITE INFOWORLD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageCell_itunes : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *image;
@property (strong, nonatomic) IBOutlet UIImageView *imageline;
@property (strong, nonatomic) IBOutlet UIButton *btnClick;
@property (strong, nonatomic) IBOutlet UILabel *lblname;
@property (strong, nonatomic) IBOutlet UILabel *lblfulltile;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activity;
@end
