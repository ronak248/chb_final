//
//  TimelineFeedCell.h
//  ishto
//
//  Created by Tejas Ardeshna on 1/29/15.
//  Copyright (c) 2015 elite infoworld. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TimelinefeedController.h"
@interface TimelineFeedCell : UITableViewCell
{

    
    IBOutlet UILabel *lbltimepost;

    IBOutlet UIButton *btntotallikes;
    IBOutlet UILabel *lbllocation;
    IBOutlet UILabel *lbldatetime;
    IBOutlet UILabel *lbleventtile;
    IBOutlet UILabel *lblday;
    IBOutlet UILabel *lblmonth;
    IBOutlet UIImageView *imgPic;

    IBOutlet UIButton *btntcomment;
    
    TimelinefeedController *ParentController;

    
}

@property (strong, nonatomic) IBOutlet UIButton *btnlikes;

@property (strong, nonatomic) IBOutlet UIButton *btncomment;
@property (strong, nonatomic) IBOutlet UIButton *btnshare;


- (IBAction)btnOffermassClicked:(id)sender;
- (IBAction)btnLikesClicked:(id)sender;
- (IBAction)btnLikeClicked:(id)sender;
- (IBAction)btncommentsClicked:(id)sender;
- (IBAction)btnshareClicked:(id)sender;
-(void)SetParent:(TimelinefeedController*)sender;
-(void)ConfigureCellWithDictionary:(NSMutableDictionary *)dictionary fori:(int)index;
@end
