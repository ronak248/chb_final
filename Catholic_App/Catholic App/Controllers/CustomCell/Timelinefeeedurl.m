//
//  Timelinefeeedurl.m
//  ishto
//
//  Created by Tejas Ardeshna on 1/29/15.
//  Copyright (c) 2015 elite infoworld. All rights reserved.
//

#import "Timelinefeeedurl.h"
#import "JSON.h"
@implementation Timelinefeeedurl

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)btnOffermassClicked:(id)sender {
}

- (IBAction)btnLikesClicked:(id)sender {
}

- (IBAction)btnLikeClicked:(id)sender {
}

- (IBAction)btncommentsClicked:(id)sender {
}

- (IBAction)btnshareClicked:(id)sender {
}

-(void)SetParent:(TimelinefeedController*)sender{
    
    
    
    
    if ([sender isKindOfClass:[TimelinefeedController class]]) {
        ParentController = sender;
    }else{
        ParentController = nil;
    }
    
    
    
    
}
-(void)ConfigureCellWithDictionary:(NSMutableDictionary *)dictionary fori:(int)index
{
    
    //2015-09-22 05:10:31 PM
    
    
        lbleventtile.text=[dictionary valueForKey:@"Title"];
        lbllocation.text=[dictionary valueForKey:@"Location"];
        
        NSString *strdate=[dictionary valueForKey:@"SelectDate"];

        NSDateFormatter *dateformat=[[NSDateFormatter alloc]init];
        [dateformat setDateFormat:@"YYYY-dd-MM hh:mm:ss a"];
        
        int likes=[[dictionary valueForKey:@"like"] intValue];
        int comments=[[dictionary valueForKey:@"comment"] intValue];
        NSString *strlikes;
        NSString *strcomments;
        if (likes==0||likes==1) {
            
            strlikes=[NSString stringWithFormat:@"%d like",likes];
        }
        else
            strlikes=[NSString stringWithFormat:@"%d likes",likes];
        
        if (comments==0||comments==1) {
            
            strcomments=[NSString stringWithFormat:@"%d comment",likes];
        }
        else
            strcomments=[NSString stringWithFormat:@"%d comments",likes];
        
        
        [btntotallikes setTitle:strlikes forState:UIControlStateNormal];
        
        [btntcomment setTitle:strcomments forState:UIControlStateNormal];


        
        NSDateFormatter *dateformat1=[[NSDateFormatter alloc]init];
        [dateformat1 setDateFormat:@"dd-MMM-YYYY HH:mm"];
        
        NSString *strhoursago=[dictionary valueForKey:@"createdAt"];

        strhoursago=[Appdelegate HourCalculation:strhoursago];
        lbltimepost.text=strhoursago;
        
    
        
        
        
    
    NSString *strsoundcloud=nil;
    NSURL *urlimge=nil;

  
    
    if([[dictionary valueForKey:@"type"] intValue]==1)
    {
        PFFile *thumbnail = [dictionary objectForKey:@"Eimage"];
        NSString *url=[thumbnail url];
        [imgPic setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"trackplaceholder-1.png"]];
        self.lbldescription.text=[dictionary valueForKey:@"Status"];
        lblTitle.text=@"CatholicHUB posted a picture";
        imgplaytunes.hidden=true;
        imgPic.hidden=false;
        self.wbView.hidden=true;
        
    }

    
     else if([[dictionary valueForKey:@"type"] intValue]==2)
    {
        
        
        lblTitle.text=@"CatholicHUB posted a link";

        imgPic.hidden=true;
        self.wbView.hidden=false;
        [self.wbView.scrollView setScrollEnabled:FALSE];
        self.lbldescription.text=[dictionary valueForKey:@"Status"];
        imgplaytunes.hidden=true;
        strsoundcloud=[Appdelegate soundlinkget:self.lbldescription.text];
        [self.wbView setOpaque:NO];
        self.wbView.backgroundColor = [UIColor clearColor];
        [self.wbView scalesPageToFit];
        [self.wbView.scrollView setScrollEnabled:FALSE];

        NSString *youTubeToken=[Appdelegate youtubelinkget:self.lbldescription.text];
        NSString *strhtml;
        if (youTubeToken!=nil)
        {
            strhtml=  [NSString stringWithFormat:@"<iframe id=\"ytplayer\" class=\"youtube-player\" type=\"text/html\" width=\"/%lf\" height=\"%lf\" src=\"http://www.youtube.com/embed/%@\" frameborder=\"0\"/>",[UIScreen mainScreen].bounds.size.width,self.wbView.frame.size.height,youTubeToken];
        }
        if(youTubeToken==nil&&strsoundcloud==nil)
        {
            urlimge= [Appdelegate detectlink:self.lbldescription.text];
        }
        [self.wbView setOpaque:NO];
        self.wbView.backgroundColor = [UIColor clearColor];
        
        if (youTubeToken!=nil) {
            
            
            
            
            // NSString *youTubeToken = @"k4ixAfJ1LuI";
            
            NSMutableString *html = [NSMutableString string];
            [html appendString:@"<html>"];
            [html appendString:@"<head>"];
            [html appendString:@"<style type=\"text/css\">"];
            [html appendString:@"body {"];
            [html appendString:@"background-color: transparent;"];
            [html appendString:@"color: white;"];
            [html appendString:@"margin: 0;"];
            [html appendString:@"}"];
            [html appendString:@"</style>"];
            [html appendString:@"</head>"];
            [html appendString:@"<body>"];
            [html appendFormat:@"%@",strhtml];
            [html appendString:@"</body>"];
            [html appendString:@"</html>"];
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if (youTubeToken!=nil) {
                        [self.wbView loadHTMLString:html baseURL:[NSURL URLWithString:@"youtube.com"]];
                        
                    }
                    else
                        [self.wbView loadHTMLString:html baseURL:[NSURL URLWithString:@"https://w.soundcloud.com"]];
                    
                    
                   
                });
            });
            
            
                      self.wbView.hidden=FALSE;
        }
        
        
        else if(strsoundcloud!=nil)
        {
            
            
            
            
            self.wbView.hidden=TRUE;
            
            
            
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                
                imgplaytunes.hidden=true;

                
                if (strsoundcloud!=nil) {
                    
                    
                    NSURL *url;
                    NSString* urlstr;
                    urlstr= [NSString stringWithFormat:@"http://api.soundcloud.com/resolve?client_id=5629741cb6c1199259c1283545357f4d&format=json&url=%@", strsoundcloud];
                    
                    
                    
                    url = [NSURL URLWithString:urlstr];
                    
                    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
                    [request setURL:[NSURL URLWithString:urlstr]];                  // set URL for the request
                    [request setHTTPMethod:@"GET"];
                    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
                    NSString * returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
                    
                    NSMutableDictionary *dicResult=[returnString JSONValue];
                    
                    
                    NSString *strurl=   [NSString stringWithFormat:@"%@",[dicResult valueForKey:@"artwork_url"]];
                    
                    strurl=[strurl stringByReplacingOccurrencesOfString:@"large.jpg" withString:@"t500x500.jpg"];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        
                        [imgPic setImageWithURL:[NSURL URLWithString:strurl] placeholderImage:[UIImage imageNamed:@"trackplaceholder-1.png"]];
                        imgPic.hidden=false;
                        self.wbView.hidden=true;
                        imgplaytunes.hidden=false;
                        
                        
                    });
                    
                    
                }
                
                
                
                else
                {
                    
                  
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        
                        
                        
                    });
                    
            
                    
                }
                
            });
            
            
            
    
        }
        else if(urlimge!=nil)
        {
            
            NSURLRequest *request = [NSURLRequest requestWithURL:urlimge];
           // [webView setScalesPageToFit:YES];
            [self.wbView loadRequest:request];
            
        }
        
        
        
        
        // viewComments.hidden=TRUE;
        
        
        //}
        
        
        
        
    }
    
    
    
}

@end
