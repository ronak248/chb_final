//
//  Timelinefeeedurl.h
//  ishto
//
//  Created by Tejas Ardeshna on 1/29/15.
//  Copyright (c) 2015 elite infoworld. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KILabel.h"
#import "TimelinefeedController.h"

@interface Timelinefeeedurl : UITableViewCell
{

    
    IBOutlet UILabel *lbltimepost;

    IBOutlet UIImageView *imgplaytunes;
    IBOutlet UIButton *btntotallikes;
    IBOutlet UILabel *lbllocation;
    IBOutlet UILabel *lbldatetime;
    IBOutlet UILabel *lbleventtile;
    IBOutlet UILabel *lblday;
    IBOutlet UILabel *lblmonth;
    IBOutlet UIImageView *imgPic;
    TimelinefeedController *ParentController;
    IBOutlet UILabel *lblTitle;

    IBOutlet UIButton *btntcomment;
}
@property (strong, nonatomic) IBOutlet UIWebView *wbView;
@property (strong, nonatomic) IBOutlet KILabel *lbldescription;
@property (strong, nonatomic) IBOutlet UIButton *btnlike;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *btncomment;
@property (strong, nonatomic) IBOutlet UIButton *btnShare;

- (IBAction)btnOffermassClicked:(id)sender;
- (IBAction)btnLikesClicked:(id)sender;
- (IBAction)btnLikeClicked:(id)sender;
- (IBAction)btncommentsClicked:(id)sender;
- (IBAction)btnshareClicked:(id)sender;
-(void)SetParent:(TimelinefeedController*)sender;
-(void)ConfigureCellWithDictionary:(NSMutableDictionary *)dictionary fori:(int)index;

@end
