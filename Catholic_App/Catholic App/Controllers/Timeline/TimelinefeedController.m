//
//  TimelinefeedController.m
//  Catholic HUB
//
//  Created by Ronak on 12/09/15.
//  Copyright (c) 2015 ELITE INFOWORLD. All rights reserved.
//

#import "TimelinefeedController.h"
#import "TimelineFeedCell.h"
#import "Timelinefeeedurl.h"
@interface TimelinefeedController ()

@end

@implementation TimelinefeedController

- (void)viewDidLoad {
    
    textViews=[[NSMutableDictionary alloc]init];
    tblView.hidden=true;
    [Appdelegate ShowAnimatedActivityInd];
    PFQuery *query = [PFQuery queryWithClassName:@"EventBookFeed"];
    [query orderByDescending:@"createdAt"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        [Appdelegate HideAnimatedActivityInd];
        arrayFeed=(NSMutableArray*)objects;
        tblView.hidden=false;
        [Appdelegate HideAnimatedActivityInd];
        [tblView reloadData];
    }];

    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - uitableview delegates -
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [arrayFeed count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
  
        return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    return UITableViewAutomaticDimension;
    
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    NSMutableDictionary *dicdata=[arrayFeed objectAtIndex:indexPath.row];
    
    
    
    
    
    if ([[dicdata valueForKey:@"type"] intValue]==3) {
        
 
        static NSString *cellId=@"TimelineFeedCell";
        
        TimelineFeedCell *cell;
        
        cell=[tableView dequeueReusableCellWithIdentifier:cellId];
        if (cell==nil)
        {
            UIViewController *tempView=[[UIViewController alloc]initWithNibName:cellId bundle:nil];
            cell=(TimelineFeedCell *)tempView.view;
            cell.accessoryType=UITableViewCellAccessoryNone;
        }
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        [cell ConfigureCellWithDictionary:dicdata fori:(int)indexPath.row];

        cell.btnlikes.tag=indexPath.row;
        [cell.btnlikes addTarget:self action:@selector(btnLikeClicked:) forControlEvents:UIControlEventTouchUpInside];
        cell.btncomment.tag=indexPath.row;
        [cell.btncomment addTarget:self action:@selector(btnCommentClicked:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnshare.tag=indexPath.row;
        [cell.btnshare addTarget:self action:@selector(btnShareClicked:) forControlEvents:UIControlEventTouchUpInside];

        
        [cell setNeedsDisplay];
        [cell layoutSubviews];
        [cell layoutIfNeeded];
        
        
        
        return cell;
        
    }
    else {
            
            
            static NSString *cellId=@"Timelinefeeedurl";
            
            Timelinefeeedurl *cell;
            
            cell=[tableView dequeueReusableCellWithIdentifier:cellId];
            if (cell==nil)
            {
                UIViewController *tempView=[[UIViewController alloc]initWithNibName:cellId bundle:nil];
                cell=(Timelinefeeedurl *)tempView.view;
                cell.accessoryType=UITableViewCellAccessoryNone;
            }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;

        NSString *strtag=[NSString stringWithFormat:@"%d",indexPath.row];
        [textViews setObject:cell.lbldescription forKey:strtag];
        [cell SetParent:self];
        [cell ConfigureCellWithDictionary:dicdata fori:(int)indexPath.row];
            [cell setNeedsDisplay];
            [cell layoutSubviews];
            [cell layoutIfNeeded];
            
            
            
            return cell;
            
        }
    return nil;

 
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - button methods

- (IBAction)btnMenuClicked:(id)sender {
    
    
        [self presentLeftMenuViewController:nil];
        
}
-(IBAction)btnLikeClicked:(id)sender
{
   int tag=[sender tag];
    
    NSMutableDictionary *dicdata=[arrayFeed objectAtIndex:tag];
    
    
    NSString *strid=[[NSUserDefaults standardUserDefaults]valueForKey:@"uid"];
        PFObject *recipe = [PFObject objectWithClassName:@"LikeFeed"];
        [recipe setObject:[dicdata valueForKey:@"objectId"] forKey:@"Eid"];
        [recipe setObject:strid forKey:@"userid"];

        //2
        
  
        
        
        [Appdelegate ShowAnimatedActivityInd];
        
        //3
        [recipe saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [Appdelegate HideAnimatedActivityInd];
                
            });
            if (error){
                
                NSString *errorString = [[error userInfo] objectForKey:@"error"];
                UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:@"Error" message:errorString delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [errorAlertView show];
                
                
                
                //...handle errors
            }
            else
            {
                
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Successfully added event" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
                [self.navigationController popViewControllerAnimated:YES];
                
                
                
            }
        }];
        
        
        


}
-(IBAction)btnCommentClicked:(id)sender
{

}
-(IBAction)btnShareClicked:(id)sender
{


    

}


@end
