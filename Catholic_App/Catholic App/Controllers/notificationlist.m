//
//  notificationlist.m
//  Ishto
//
//  Created by Tejas Ardeshna on 3/2/15.
//  Copyright (c) 2015 elite infoworld. All rights reserved.
//

#import "notificationlist.h"
#import "chatRoomCell.h"
#import <Parse/Parse.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "detaIls.h"
@interface notificationlist ()

@end

@implementation notificationlist

- (void)viewDidLoad
{
    
    sliderproximaty.value=0;
    sliderproximaty.maximumValue=100.0;
    lblvalues.text=@"0 mile";

    viewheader.layer.borderWidth=4.0f;
    [viewheader.layer setCornerRadius:10.0f];
    [super viewDidLoad];
    pageUser=0;
    pageTitle=0;
    pageHashTag=0;
    
  
    [Appdelegate ShowAnimatedActivityInd];

    
    //[self.view addSubview:self.control];
    
    [txtSearch becomeFirstResponder];
    
//    _control.frame=CGRectMake(0, 64, [UIScreen mainScreen].bounds.size.width, 60);
//    //
//    for (UIView *subView in objSearchBar.subviews)
//    {
//    
//        for (UIView *secondLevelSubview in subView.subviews){
//        
//            if ([secondLevelSubview isKindOfClass:[UITextField class]])
//            {
//            
//                UITextField *searchBarTextField = (UITextField *)secondLevelSubview;
//                
//                //set font color here
//                searchBarTextField.textColor = [UIColor blackColor];
//                
//                break;
//                
//            }
//            
//        }
//        
//    }

    //_control.height=50;
    //[objSearchBar setTintColor:[UIColor blackColor]];
    arrProduct=[[NSMutableArray alloc]init];
        arrUserList=[[NSMutableArray alloc]init];
    // Do any additional setup after loading the view.
    lblNoData=[[UILabel alloc]initWithFrame:CGRectMake(0, 10, [UIScreen mainScreen].bounds.size.width, 30)];
    lblNoData.textAlignment=NSTextAlignmentCenter;
    lblNoData.text=@"No Data Found";
    [tblView addSubview:lblNoData];
    //[colView addSubview:lblNoData];
    lblNoData.hidden=true;
    [self.view bringSubviewToFront:lblNoData];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"pushNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushNotificationReceived:) name:@"pushNotification" object:nil];
    [super viewWillAppear:YES];
    
    



}
-(void)viewDidAppear:(BOOL)animated
{
    

    
    PFQuery *query = [PFQuery queryWithClassName:@"notification"];
    [query orderByDescending:@"createdAt"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        [Appdelegate HideAnimatedActivityInd];
        arrUserList=(NSMutableArray*)objects;
        [tblView reloadData];
    }];
    
}

-(void)viewWillAppear:(BOOL)animated
{
 
}
-(void)viewWillDisappear:(BOOL)animated
{
   
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"pushNotification" object:nil];
    
    
}
-(IBAction)backButtonClicked:(id)sender
{


    [self.navigationController popViewControllerAnimated:YES];


}
-(IBAction)btnsearchClicked:(id)sender
{


    pageUser=1;
    
    [self getData];
    [self.view endEditing:YES];
    

}
-(IBAction)btnSliderchangeClicked:(id)sender
{


  lblvalues.text=[NSString stringWithFormat:@"%0.01lf miles",sliderproximaty.value];


}

- (IBAction)btnmenuClicked:(id)sender {
    
    [self presentLeftMenuViewController:nil];

}
+ (void)load
{
    
//    [[DZNSegmentedControl appearance] setBackgroundColor:[UIColor whiteColor]];
//    [[DZNSegmentedControl appearance] setTintColor:[UIColor colorFromHex:0x01d48f]];
//    [[DZNSegmentedControl appearance] setHairlineColor:[UIColor colorFromHex:0xb9c3c3]];
//    
//    [[DZNSegmentedControl appearance] setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:14.0]];
//    [[DZNSegmentedControl appearance] setSelectionIndicatorHeight:2.5];
//    [[DZNSegmentedControl appearance] setAnimationDuration:0.25];
//    
//    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor], NSFontAttributeName: [UIFont systemFontOfSize:15.0]}];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - table view delegates -
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    int total_height=40;
    
//    if ([UIScreen mainScreen].bounds.size.width==320.0) {
//        total_height=10.0;
//    }
//    else
//     total_height=40;

    
    NSString *strtext=[[arrUserList objectAtIndex:indexPath.row]valueForKey:@"notificationtext"];
    
    UILabel *tempTV = [[UILabel alloc] init];
    [tempTV setFont:[UIFont fontWithName:@"Helvetica Neue LT Std" size:13]];

    [tempTV setText:strtext];

    
    tempTV.frame=CGRectMake(70, 48, [UIScreen mainScreen].bounds.size.width-100, 50);

   CGSize sizeDesc = [tempTV sizeThatFits:CGSizeMake([UIScreen mainScreen].bounds.size.width-100, FLT_MAX)];
    total_height = total_height + sizeDesc.height;
    
    if (total_height>=51) {
        return 115;
    }
    
    return total_height+25;;
    
}
//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    
//    return UITableViewAutomaticDimension;
//    
//    
//    
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    
        return arrUserList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellUser";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        UILabel * celllabel3 = [[UILabel alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-120, 15, 100, 20)];
        [celllabel3 setTextAlignment:NSTextAlignmentRight];
        celllabel3.backgroundColor= [UIColor clearColor];
        celllabel3.tag=1002;
        [celllabel3 setFont:[UIFont fontWithName:@"Helvetica Neue LT Std" size:12.0]];
        
        [cell.contentView addSubview:celllabel3];
        
        UILabel * celllabel4 = [[UILabel alloc] initWithFrame:CGRectMake(70, 15, [UIScreen mainScreen].bounds.size.width-155, 20)];
        [celllabel4 setTextAlignment:NSTextAlignmentLeft];
        celllabel4.backgroundColor= [UIColor clearColor];
        celllabel4.tag=1003;
        [celllabel4 setFont:[UIFont fontWithName:@"Helvetica Neue LT Std" size:14]];
        
        [cell.contentView addSubview:celllabel4];
        
        UILabel * celllabel5 = [[UILabel alloc] initWithFrame:CGRectMake(70, 20, [UIScreen mainScreen].bounds.size.width-80, 60)];
        [celllabel5 setTextAlignment:NSTextAlignmentLeft];
        celllabel5.backgroundColor= [UIColor clearColor];
        celllabel5.tag=1004;
        [celllabel5 setFont:[UIFont fontWithName:@"Helvetica Neue LT Std" size:13]];
        
        [cell.contentView addSubview:celllabel5];
        
        
        UIImageView * cellimg = [[UIImageView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-40, 30, 34, 34)];
        cellimg.tag=1009;
        
        //[cell.contentView addSubview:cellimg];

        

    }
    
    

    UILabel* celllabel3 = (UILabel*)[cell.contentView viewWithTag:1002];
    UILabel* celllabel4 = (UILabel*)[cell.contentView viewWithTag:1003];
    UILabel* celllabel5 = (UILabel*)[cell.contentView viewWithTag:1004];
   // UIImageView* cellimg = (UIImageView*)[cell.contentView viewWithTag:1009];

    [celllabel3 setTextColor:[UIColor darkGrayColor]];
    [celllabel4 setTextColor:[UIColor blackColor]];
    [celllabel5 setTextColor:[UIColor darkGrayColor]];


    [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica Neue LT Std" size:14]];
    [cell.textLabel setTextColor:[UIColor blackColor]];
    [cell.detailTextLabel setTextColor:[UIColor darkGrayColor]];
    cell.detailTextLabel.numberOfLines=3;
    cell.selectionStyle=UITableViewCellSeparatorStyleNone;
    celllabel4.text=[NSString stringWithFormat:@"%@",[[arrUserList objectAtIndex:indexPath.row]valueForKey:@"title"]];

    
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    
    NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
    [dateformatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];

    [dateformatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    
    NSString *strdatee1=[NSString stringWithFormat:@"%@",[[arrUserList objectAtIndex:indexPath.row]valueForKey:@"createdAt"]];

    NSArray *array=[strdatee1 componentsSeparatedByString:@"+"];
    
    strdatee1=[array objectAtIndex:0];
    NSDate *strdate=[dateformatter dateFromString:strdatee1];

    
    if ([[[arrUserList objectAtIndex:indexPath.row] valueForKey:@"read"] isEqualToString:@"1"]) {
        
        cell.imageView.image=[UIImage imageNamed:@"unread"];
        
    }
    else
        cell.imageView.image=[UIImage imageNamed:@"read"];

    
    
    NSDateFormatter *datefromatter1=[[NSDateFormatter alloc]init];
    [datefromatter1 setDateFormat:@"E, dd MMM"];
    
    NSString *strdatee=[datefromatter1 stringFromDate:strdate];

    
    
    celllabel3.text=[NSString stringWithFormat:@"%@",strdatee];
    
    [celllabel3 setTextColor:[UIColor colorFromHex:0x10478A]];
    int total_height=20;
    NSString *strtext=[[arrUserList objectAtIndex:indexPath.row]valueForKey:@"notificationtext"];
    


    CGSize maximumLabelSize = CGSizeMake([UIScreen mainScreen].bounds.size.width-80,9999);
   CGSize expectedLabelSize = [strtext sizeWithFont:[UIFont fontWithName:@"Helvetica Neue LT Std" size:14] constrainedToSize:maximumLabelSize lineBreakMode:UILineBreakModeWordWrap]; //iOS 6 and previous.
    total_height = total_height + expectedLabelSize.height;
    
    if (total_height>=46) {

        celllabel5.frame=CGRectMake(70, 48, [UIScreen mainScreen].bounds.size.width-100, 50);

    }
    else
        celllabel5.frame=CGRectMake(70, 43, [UIScreen mainScreen].bounds.size.width-100, 30);

    
    celllabel5.numberOfLines=3;
    
   // cell.imageView.image=[UIImage imageNamed:@"iconp"];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:strtext];
    
    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Helvetica Neue LT Std" size:13] range:NSMakeRange(0, strtext.length)];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:2];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [strtext length])];
    celllabel5.attributedText=attributedString;

    
        return cell;
        
        
    
    

}
#pragma mark - table view delegates -
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    
    return nil;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section;
{
    
    
    return 0.0;
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{



    PFQuery *query = [PFQuery queryWithClassName:@"notification"];
    
    NSString *strobjectid=[[arrUserList objectAtIndex:indexPath.row] objectId];
    
    [query whereKey:@"objectId" equalTo:strobjectid];
    [query getFirstObjectInBackgroundWithBlock:^(PFObject * reportStatus, NSError *error)        {
        if (!error) {
            
            // Found UserStats
            [reportStatus setObject:@"0" forKey:@"read"];
            
            [reportStatus saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {

            }];

        }
    
    }];

    detaIls *objdt=[Appdelegate.storyboard instantiateViewControllerWithIdentifier:@"detaIls"];
    
    objdt.strtitle=[[arrUserList objectAtIndex:indexPath.row]valueForKey:@"title"];
    objdt.strdescription=[[arrUserList objectAtIndex:indexPath.row]valueForKey:@"notificationtext"];

    NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
    [dateformatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    
    [dateformatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    
    NSString *strdatee1=[NSString stringWithFormat:@"%@",[[arrUserList objectAtIndex:indexPath.row]valueForKey:@"createdAt"]];
    
    NSArray *array=[strdatee1 componentsSeparatedByString:@"+"];
    
    strdatee1=[array objectAtIndex:0];
    NSDate *strdate=[dateformatter dateFromString:strdatee1];
    
    
    
    NSDateFormatter *datefromatter1=[[NSDateFormatter alloc]init];
    [datefromatter1 setDateFormat:@"E, dd MMM YYYY"];
    
    NSString *strdatee=[datefromatter1 stringFromDate:strdate];
    objdt.strdate=strdatee;
    
    [self.navigationController pushViewController:objdt animated:YES];
    
    
}
#pragma mark - search bar delegates -

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{

    
        pageUser=1;
        
        [self getData];
        [self.view endEditing:YES];
        
 
    
    return true;

}
-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    for (UIView *subview in searchBar.subviews)
    {
        if ([subview isKindOfClass:[UIButton class]])
        {
            int64_t delayInSeconds = .001;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                UIButton * cancelButton = (UIButton *)subview;
                [cancelButton setEnabled:YES];
            });
            break;
        }
    }
}
-(void)getData
{
    
  

   
        
    

}

#pragma mark - json parsign -

@end
