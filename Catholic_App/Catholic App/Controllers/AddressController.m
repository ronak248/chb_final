//
//  AddressController.m
//  Catholic HUB
//
//  Created by Ronak on 26/07/15.
//  Copyright (c) 2015 ELITE INFOWORLD. All rights reserved.
//

#import "AddressController.h"

@interface AddressController ()

@end

@implementation AddressController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(IBAction)backButtonClicked:(id)sender
{
    
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
