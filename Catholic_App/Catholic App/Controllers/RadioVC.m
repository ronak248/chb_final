//
//  RadioVC.m
//  Catholic App
//
//  Created by Ronak on 19/06/15.
//  Copyright (c) 2015 ELITE INFOWORLD. All rights reserved.
//

#import "RadioVC.h"
#import "AudioStreamer.h"
#import "PaymentVC.h"
#import "RequestEmail.h"
#import "privatebrowser.h"
#import <Social/Social.h>
#import "notificationlist.h"
@interface RadioVC ()

@end

@implementation RadioVC

- (void)viewDidLoad {
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(radiostart) name:@"radiostart" object:nil];

    
    if (Appdelegate.isvisiblelanguage) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"removetab" object:nil];
        viewbg.hidden=false;
        Appdelegate.isvisiblelanguage=false;

    }
    else
    {
    
    
        [[NSNotificationCenter defaultCenter] postNotificationName:@"addtab" object:nil];
        viewbg.hidden=true;
    }

    
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"chb" ofType:@"html" inDirectory:nil];
    NSData *htmlData = [NSData dataWithContentsOfFile:htmlFile];
    [wvPreview loadData:htmlData MIMEType:@"text/html" textEncodingName:@"UTF-8" baseURL:[NSURL URLWithString:@""]];
    [super viewDidLoad];

    slidervoulme.value=0.7;
    
    //[self createStreamer:KstreamUrl];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidAppear:(BOOL)animated
{

    



    
    if (Appdelegate.strstreamlink) {
        [self destroyStreamer];
        
        
        btnplay.tag=0;
        // [btnplay setImage:[UIImage imageNamed:@"pause"] forState:UIControlStateNormal];
        imgplay.image=[UIImage imageNamed:@"pause"];
        
        [self createStreamer:Appdelegate.strstreamlink];
    }
    

   

    


}

-(void)radiostart
{

    
    [self destroyStreamer];

        btnplay.tag=0;
    imgplay.image=[UIImage imageNamed:@"pause"];
    
    [self createStreamer:Appdelegate.strstreamlink];
    
    

}

-(void)viewWillDisappear:(BOOL)animated
{


    [self destroyStreamer];


}
- (void)createStreamer:(NSString *)strurl
{
    
    
    

    NSString *escapedValue =
    (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                         nil,
                                                         (CFStringRef)strurl,
                                                         NULL,
                                                         NULL,
                                                         kCFStringEncodingUTF8));
    NSURL *url = [NSURL URLWithString:escapedValue];
    
    streamer = [[AudioStreamer alloc] initWithURL:url];
    
    [streamer start];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:nil
     name:ASStatusChangedNotification
     object:streamer];
    
    
}

- (void)destroyStreamer
{
    
    if (streamer)
    {
        [[NSNotificationCenter defaultCenter]
         removeObserver:self
         name:ASStatusChangedNotification
         object:streamer];
        [streamer stop];
        streamer = nil;
    }
}
#pragma custom buttons methods
-(IBAction)btnpaysponserClicked:(id)sender
{

    
    PaymentVC *objvc=[Appdelegate.storyboard instantiateViewControllerWithIdentifier:@"PaymentVC"];
    [self.navigationController pushViewController:objvc animated:YES];




}

-(IBAction)btnrefreshClicked:(id)sender
{

    [self radiostart];


}

- (IBAction)btnbackClicked:(id)sender {
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"removetab" object:nil];

    
    
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.7;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.layer addAnimation:transition forKey:nil];
    [self.view addSubview:viewbg];
    [self destroyStreamer];


}

- (IBAction)btnnotificationClikced:(id)sender {
    
    
    notificationlist *objnoti=[self.storyboard instantiateViewControllerWithIdentifier:@"notificationlist"];
    [self.navigationController pushViewController:objnoti animated:YES];
    
}

- (IBAction)btnteluguClicked:(id)sender {
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.7;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.layer addAnimation:transition forKey:nil];
    
    [viewbg removeFromSuperview];
    [self destroyStreamer];
    
    
    btnplay.tag=0;
    // [btnplay setImage:[UIImage imageNamed:@"pause"] forState:UIControlStateNormal];
    imgplay.image=[UIImage imageNamed:@"pause"];
    
    Appdelegate.strstreamlink=KtelugustreamUrl;
    
    [self createStreamer: Appdelegate.strstreamlink];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"addtab" object:nil];

}

- (IBAction)btnEnglishClicked:(id)sender {
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.7;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.layer addAnimation:transition forKey:nil];
    [viewbg removeFromSuperview];

    [self destroyStreamer];
    
    
    btnplay.tag=0;
    // [btnplay setImage:[UIImage imageNamed:@"pause"] forState:UIControlStateNormal];
    imgplay.image=[UIImage imageNamed:@"pause"];
    Appdelegate.strstreamlink=KenglishstreamUrl;

    [self createStreamer: Appdelegate.strstreamlink];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"addtab" object:nil];

}

- (IBAction)btntamilClicked:(id)sender {
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.7;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.layer addAnimation:transition forKey:nil];
    [viewbg removeFromSuperview];

    
    [self destroyStreamer];
    
    
    btnplay.tag=0;
    // [btnplay setImage:[UIImage imageNamed:@"pause"] forState:UIControlStateNormal];
    imgplay.image=[UIImage imageNamed:@"pause"];
    
    
    Appdelegate.strstreamlink=KtamilstreamUrl;
    
    [self createStreamer: Appdelegate.strstreamlink];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"addtab" object:nil];

}

- (IBAction)btnmenuClicked:(id)sender {
    
    [self presentLeftMenuViewController:nil];

}

-(IBAction)playClicked:(id)sender
{
    
    if (streamer.isPlaying) {
        
        [streamer pause];
        btnplay.tag=1;
        imgplay.image=[UIImage imageNamed:@"play"];

    }
    
    else if (streamer.isIdle)
    {
        [streamer start];
        btnplay.tag=0;
        imgplay.image=[UIImage imageNamed:@"pause"];

        
    
    }
    else if (streamer==nil)
    {
        [self destroyStreamer];

        [self createStreamer:Appdelegate.strstreamlink];
        btnplay.tag=0;
        imgplay.image=[UIImage imageNamed:@"pause"];

        
        
    }
    else if(streamer.isWaiting)
    {
        
        // UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"start from waiting" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        
        //[alert show];
        if (streamer) {
            [self destroyStreamer];
            
            
        }
        btnplay.tag=1;
        imgplay.image=[UIImage imageNamed:@"play"];

        
        //[self createStreamer:KstreamUrl];
       // [streamer stop];
        
        
    }
    

    else
    {        [streamer start];
        btnplay.tag=0;
        imgplay.image=[UIImage imageNamed:@"pause"];
        
    
    }


}

-(IBAction)btnrequestClicked:(id)sender
{

    privatebrowser *objweb=[Appdelegate.storyboard instantiateViewControllerWithIdentifier:@"privatebrowser"];
    
    
    objweb.strweblink=[NSURL URLWithString:@"http://www.catholichub.tv/prayer-request/"];
    
    [self.navigationController pushViewController:objweb animated:YES];
    
   // RequestEmail *obj=[Appdelegate.storyboard instantiateViewControllerWithIdentifier:@"RequestEmail"];
    
    //[self.navigationController pushViewController:obj animated:YES];
//    MFMailComposeViewController *mailComposeViewController = [[MFMailComposeViewController alloc] init];
//    
//    // only on iOS < 3
//    //if ([MFMailComposeViewController canSendMail] == NO)
//    //  [self launchMailApp]; // you need to
//    
//    mailComposeViewController.mailComposeDelegate = self;
//    [mailComposeViewController setToRecipients:[NSArray arrayWithObjects:@"catholichubtv@gmail.com",nil]];
//    [mailComposeViewController setSubject:@""];
//    [mailComposeViewController setMessageBody:@"" isHTML:YES];
//    mailComposeViewController.delegate = self;
//    [self.navigationController presentViewController:mailComposeViewController animated:YES completion:nil];

    


}


- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result)
    {
        case MessageComposeResultCancelled:
        {
            //NSLog(@"Cancelled");
            
        }
            
            break;
        case MessageComposeResultFailed:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Fail" message:@"Message Fail to send"
                                                           delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
            break;
            
        case MessageComposeResultSent:
            //NSLog(@"Message Sent Successfully");
            break;
        default:
            break;
    }
    [self dismissViewControllerAnimated:NO completion:nil];
    
}
- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent)
    {
        //NSLog(@"It's away!");
    }
    [self dismissViewControllerAnimated:NO completion:nil];
}


-(IBAction)sliderchange:(id)sender
{

NSString *strvalue=[NSString stringWithFormat:@"%f",slidervoulme.value];
    
    [streamer setVolume:slidervoulme.value];
    

}
-(IBAction)btnregisterClicked:(id)sender
{

    
    privatebrowser *objweb=[Appdelegate.storyboard instantiateViewControllerWithIdentifier:@"privatebrowser"];
    
    objweb.strweblink=[NSURL URLWithString:@"http://member.catholichub.org/register.php"];
    
    [self.navigationController pushViewController:objweb animated:YES];
    
    
    

    

}

-(IBAction)btnfbshare:(id)sender
{
    
        if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
            
            SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
            [controller setTitle:@"CatholicHUB Radio and TV app"];
            [controller setInitialText:@"CatholicHUB LIVE TV and Radio App, Streams Catholic Hymns, Prayers, Talks and News in Telugu, English and Tamil, which takes people closer to God."];
            [controller addURL:[NSURL URLWithString:@"http://catholichub.tv"]];
            [controller addImage:[UIImage imageNamed:@"iconp"]];
            [self presentViewController:controller animated:YES completion:Nil];
            
            SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
                NSString *output= nil;
                switch (result) {
                    case SLComposeViewControllerResultCancelled:
                        output= @"Action Cancelled";
                        NSLog (@"cancelled");
                        break;
                    case SLComposeViewControllerResultDone:
                        output= @"Post Succesfull";
                        NSLog (@"success");
                        break;
                    default:
                        break;
                }
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook" message:output delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
                [controller dismissViewControllerAnimated:YES completion:Nil];
            };
            controller.completionHandler =myBlock;
        
    
    
//    
//    
//    
//
//    if (FBSession.activeSession.state == FBSessionStateOpen
//        || FBSession.activeSession.state == FBSessionStateOpenTokenExtended) {
//        
//        // Close the session and remove the access token from the cache
//        // The session state handler (in the app delegate) will be called automatically
//        [FBSession openActiveSessionWithReadPermissions:@[@"basic_info"]
//                                           allowLoginUI:YES
//                                      completionHandler:
//         ^(FBSession *session, FBSessionState state, NSError *error)
//         {
//             
//             // Retrieve the app delegate
//             // AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
//             // Call the app delegate's sessionStateChanged:state:error method to handle session state changes
//             [self sessionStateChanged:session state:state error:error];
//         }];
//        
//        // If the session state is not any of the two "open" states when the button is clicked
//    }
//    else
//    {
//        // Open a session showing the user the login UI
//        // You must ALWAYS ask for basic_info permissions when opening a session
//        
//        [FBSession openActiveSessionWithReadPermissions:@[@"public_profile"]
//                                           allowLoginUI:YES
//                                      completionHandler:
//         ^(FBSession *session, FBSessionState state, NSError *error)
//         {
//             
//             [self sessionStateChanged:session state:state error:error];
//         }];
//    }
//
//
        }
}
//- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error
//{
//    // If the session was opened successfully
//    if (!error && state == FBSessionStateOpen)
//    {
//        //NSLog(@"Session opened");
//        
//        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                       @"CatholicHUB Radio and TV app",@"name",
//                                       @"CatholicHUB LIVE TV and Radio App, Streams Catholic Hymns, Prayers, Talks and News in Telugu, English and Tamil, which takes people closer to God.", @"description",
//                                       @"http://catholichub.tv", @"link",
//                                       @"http://catholichub.tv/upload/chub1.png", @"picture",
//
//                                       nil];
//        
//        [FBWebDialogs presentFeedDialogModallyWithSession:nil
//                                               parameters:params
//                                                  handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error)
//         {
//             if (error)
//             {
//                 // An error occurred, we need to handle the error
//                 // See: https://developers.facebook.com/docs/ios/errors
//                 //NSLog([NSString stringWithFormat:@"Error publishing story: %@", error.description]);
//             }
//             else
//             {
//                 if (result == FBWebDialogResultDialogNotCompleted)
//                 {
//                     // User canceled.
//                     //NSLog(@"User cancelled.");
//                 }
//                 else
//                 {
//                     // Handle the publish feed callback
//                     NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
//                     
//                     if (![urlParams valueForKey:@"post_id"])
//                     {
//                         // User canceled.
//                         //NSLog(@"User cancelled.");
//                         
//                     } else
//                     {
//                         // User clicked the Share button
//                         [self showMessage:@"successfully shared" withTitle:@"Facebook"];
//                         NSString *result = [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]];
//                         //NSLog(@"result %@", result);
//                     }
//                 }
//             }
//         }];
//        //[self postfb];
//        // Show the user the logged-in UI
//        // [self userLoggedIn];
//        return;
//    }
//    if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed){
//        // If the session is closed
//        //NSLog(@"Session closed");
//        // Show the user the logged-out UI
//        // [self userLoggedOut];
//    }
//    
//    // Handle errors
//    if (error)
//    {
//        //NSLog(@"Error");
//        NSString *alertText;
//        NSString *alertTitle;
//        // If the error requires people using an app to make an action outside of the app in order to recover
//        if ([FBErrorUtility shouldNotifyUserForError:error] == YES)
//        {
//            alertTitle = @"Something went wrong";
//            alertText = [FBErrorUtility userMessageForError:error];
//            [self showMessage:alertText withTitle:alertTitle];
//        }
//        else
//        {
//            
//            // If the user cancelled login, do nothing
//            if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled)
//            {
//                //NSLog(@"User cancelled login");
//                
//                // Handle session closures that happen outside of the app
//            } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession){
//                alertTitle = @"Session Error";
//                alertText = @"Your current session is no longer valid. Please log in again.";
//                [self showMessage:alertText withTitle:alertTitle];
//                
//                // For simplicity, here we just show a generic message for all other errors
//                // You can learn how to handle other errors using our guide: https://developers.facebook.com/docs/ios/errors
//            } else {
//                //Get more error information from the error
//                NSDictionary *errorInformation = [[[error.userInfo objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"] objectForKey:@"body"] objectForKey:@"error"];
//                
//                // Show the user an error message
//                alertTitle = @"Something went wrong";
//                alertText = [NSString stringWithFormat:@"Please retry. \n\n If the problem persists contact us and mention this error code: %@", [errorInformation objectForKey:@"message"]];
//                [self showMessage:alertText withTitle:alertTitle];
//            }
//        }
//        // Clear this token
//        [FBSession.activeSession closeAndClearTokenInformation];
//        // Show the user the logged-out UI
//        //[self userLoggedOut];
//    }
//}
//- (NSDictionary*)parseURLParams:(NSString *)query {
//    NSArray *pairs = [query componentsSeparatedByString:@"&"];
//    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
//    for (NSString *pair in pairs) {
//        NSArray *kv = [pair componentsSeparatedByString:@"="];
//        NSString *val =
//        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//        params[kv[0]] = val;
//    }
//    return params;
//}
//- (void)showMessage:(NSString *)text withTitle:(NSString *)title
//{
//    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:title message:text delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
//    [alert show];
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
