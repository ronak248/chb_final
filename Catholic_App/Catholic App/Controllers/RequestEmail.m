//
//  RequestEmail.m
//  Catholic App
//
//  Created by Ronak on 02/07/15.
//  Copyright (c) 2015 ELITE INFOWORLD. All rights reserved.
//

#import "RequestEmail.h"
#import <MessageUI/MessageUI.h>

@interface RequestEmail ()

@end

@implementation RequestEmail

- (void)viewDidLoad {
    
    
    txtdescription.text=@"";
    txtemail.text=@"";
    txtname.text=@"";
    txtphone.text=@"";
    txtdescription.textColor=[UIColor lightGrayColor];
    txtdescription.layer.borderWidth=1.0f;
    txtdescription.layer.borderColor=[UIColor lightGrayColor].CGColor;
    [txtdescription.layer setCornerRadius:2.0f];
    txtdescription.layer.masksToBounds=true;
    
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(resignKeyboard)];
    tap.delegate=self;
    tap.cancelsTouchesInView=false;
    [self.view addGestureRecognizer:tap];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)resignKeyboard
{
    [self.view endEditing:YES];
    //[scrollView setContentOffset:CGPointZero animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btnbackClicked:(id)sender
{



    [self.navigationController popViewControllerAnimated:YES];

}

-(IBAction)btnsubmitCLicked:(id)sender
{


    if (txtname.text.length==0 || [txtname.text isEqualToString:@""])
    {
        
        [Appdelegate showAlertWithTitle:@"" andMessge:@"please enter name"];
        
    }
    else if (txtemail.text.length==0 || [txtemail.text isEqualToString:@""])
    {
        
        [Appdelegate showAlertWithTitle:@"" andMessge:@"please enter email"];
        
    }
    else if (txtphone.text.length==0 || [txtphone.text isEqualToString:@""])
    {
        
        [Appdelegate showAlertWithTitle:@"" andMessge:@"please enter phone"];
        
    }
    
    else if(txtdescription.text.length==0 || [txtdescription.text isEqualToString:@""]||[txtdescription.text isEqualToString:@"Description"])
    {
        
        [Appdelegate showAlertWithTitle:@"" andMessge:@"please enter description"];
        
    }
    else
    {
        
        MFMailComposeViewController *mailComposeViewController = [[MFMailComposeViewController alloc] init];
        NSString *strcontent=[NSString stringWithFormat:@"<p><b>Name : %@ </b></p><p><b> Email : %@</b></p><p><b>Phone : %@ <p><b><p><b> Description : %@<p><b>",txtname.text,txtemail.text,txtphone.text,txtdescription.text];
        
        mailComposeViewController.mailComposeDelegate = self;
        [mailComposeViewController setToRecipients:[NSArray arrayWithObjects:@"prayer@catholichub.org",nil]];
        [mailComposeViewController setSubject:@"Prayer Request"];
        [mailComposeViewController setMessageBody:strcontent isHTML:YES];
        mailComposeViewController.delegate = self;
        [self.navigationController presentViewController:mailComposeViewController animated:YES completion:nil];
        
        
        
    }

}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result)
    {
        case MessageComposeResultCancelled:
        {
            //NSLog(@"Cancelled");
            
        }
            
            break;
        case MessageComposeResultFailed:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Fail" message:@"Message Fail to send"
                                                           delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
            break;
            
        case MessageComposeResultSent:
            //NSLog(@"Message Sent Successfully");
            break;
        default:
            break;
    }
    [self dismissViewControllerAnimated:NO completion:nil];
    
}
- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent)
    {
        //NSLog(@"It's away!");
    }
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([textView.text isEqualToString:@"Post here..."])
    {
        textView.text=@"";
        textView.textColor=[UIColor lightGrayColor];
    }
    else if([textView.text length]==0)
    {
        
        
    }
    else
    textView.textColor=[UIColor blackColor];
    

        return YES;
   
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    
    if ([textView.text isEqualToString:@""])
    {
        
        textView.textColor=[UIColor lightGrayColor];
        
        textView.text=@"Post here...";
        
    }
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [self.view endEditing:YES];

    return true;

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
