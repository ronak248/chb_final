//
//  SideMenuViewController.m
//  MFSideMenuDemo
//
//  Created by Michael Frederick on 3/19/12.

#import "SideMenuViewController.h"
#import "RESideMenu.h"
#import "chatRoomCell.h"
#import "privatebrowser.h"
#import "PaymentVC.h"
#import "notificationlist.h"
#import "ContactUSController.h"
#import "RadioVC.h"
#import "LiveTVVC.h"
#import "itunespreviewcnt.h"
#import "LoginHomeController.h"
#import "updateProfile.h"
#import "EventDetails.h"
#import "TimelinefeedController.h"
@implementation SideMenuViewController
{

}
#pragma mark -
#pragma mark - UITableViewDataSource

-(void)viewDidLoad
{
    self.navigationController.navigationBarHidden=TRUE;

    arraymenu=[[NSMutableArray alloc]init];
    NSMutableDictionary *dicdata=[[NSMutableDictionary alloc]init];
    
    [dicdata setValue:@"Home" forKey:@"name"];
    [dicdata setValue:@"hos" forKey:@"img"];
    
    [arraymenu addObject:dicdata];

    
    dicdata=[[NSMutableDictionary alloc]init];
    
    [dicdata setValue:@"Live TV" forKey:@"name"];
    [dicdata setValue:@"los" forKey:@"img"];
    
    //[arraymenu addObject:dicdata];
    
    
    dicdata=[[NSMutableDictionary alloc]init];
    
    [dicdata setValue:@"Videos" forKey:@"name"];
    [dicdata setValue:@"vos" forKey:@"img"];
    
   // [arraymenu addObject:dicdata];
    
    
    dicdata=[[NSMutableDictionary alloc]init];
    
    [dicdata setValue:@"Prayer" forKey:@"name"];
    [dicdata setValue:@"pos" forKey:@"img"];
    
    [arraymenu addObject:dicdata];
    
    dicdata=[[NSMutableDictionary alloc]init];
    
    [dicdata setValue:@"Join Us" forKey:@"name"];
    [dicdata setValue:@"jos" forKey:@"img"];
    
    [arraymenu addObject:dicdata];
    
    dicdata=[[NSMutableDictionary alloc]init];
    
    [dicdata setValue:@"News Feed" forKey:@"name"];
    [dicdata setValue:@"nos" forKey:@"img"];
    
    [arraymenu addObject:dicdata];
    
    dicdata=[[NSMutableDictionary alloc]init];
    
    [dicdata setValue:@"Messages" forKey:@"name"];
    [dicdata setValue:@"mos" forKey:@"img"];
    
    [arraymenu addObject:dicdata];
    
    dicdata=[[NSMutableDictionary alloc]init];
    
    [dicdata setValue:@"Offer Mass" forKey:@"name"];
    [dicdata setValue:@"oos" forKey:@"img"];
    
    [arraymenu addObject:dicdata];
    
    dicdata=[[NSMutableDictionary alloc]init];
    
    [dicdata setValue:@"Events" forKey:@"name"];
    [dicdata setValue:@"eos" forKey:@"img"];
    
    [arraymenu addObject:dicdata];
    
    
    dicdata=[[NSMutableDictionary alloc]init];
    
    [dicdata setValue:@"Support Us" forKey:@"name"];
    [dicdata setValue:@"sos" forKey:@"img"];
    
    [arraymenu addObject:dicdata];
    
    dicdata=[[NSMutableDictionary alloc]init];
    
    [dicdata setValue:@"Contact Us" forKey:@"name"];
    [dicdata setValue:@"cos" forKey:@"img"];
    
    [arraymenu addObject:dicdata];
    
    dicdata=[[NSMutableDictionary alloc]init];
    
    [dicdata setValue:@"Log Out" forKey:@"name"];
    [dicdata setValue:@"logs" forKey:@"img"];
    
    [arraymenu addObject:dicdata];

    
    lblfullname.text=[Appdelegate.dicProfiledata valueForKey:@"fullname"];

   
}
-(void)viewWillAppear:(BOOL)animated
{
    
}
-(void)viewDidDisappear:(BOOL)animated
{

}

-(void)viewWillDisappear:(BOOL)animated
{
    
}
-(IBAction)homeclicked:(id)sender
{

    
    [self.sideMenuViewController hideMenuViewController];
}

-(IBAction)btnClick:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    
    if ([btn tag]==0)
    {
        
//        Appdelegate.isLoadFromSideView=YES;
//        [self.sideMenuViewController setContentViewController:[[XXNavigationController alloc] initWithRootViewController:[[PeopleListingController alloc] init]]
//                                                     animated:YES];
        [self.sideMenuViewController hideMenuViewController];
        
        
    }
    else if ([btn tag]==1)
    {
        
      
        [self.sideMenuViewController hideMenuViewController];
        
    

    }
    else if ([btn tag]==2)
    {
        
    
        [self.sideMenuViewController hideMenuViewController];
        

    }
    else if ([btn tag]==3)
    {

        [self.sideMenuViewController hideMenuViewController];
        
        
    }
    else if ([btn tag]==4)
    {
        

        [self.sideMenuViewController hideMenuViewController];
    }
    else if ([btn tag]==5)
    {
        
//        [Appdelegate disconnect];
//        NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
//        NSDictionary * dict = [defs dictionaryRepresentation];
//        for (id key in dict) {
//            [defs removeObjectForKey:key];
//        }
//        [defs synchronize];
//        LoginController *objlogin=[[LoginController alloc]initWithNibName:@"LoginController" bundle:nil];
//        
//      UINavigationController*  navigationController=[[UINavigationController alloc]initWithRootViewController:objlogin];
//        
//        
//        navigationController.navigationBarHidden=true;
//        SideMenuViewController *leftViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController" bundle:nil];
//        RESideMenu *sideMenuViewController = [[RESideMenu alloc] initWithContentViewController:navigationController
//                                                                        leftMenuViewController:leftViewController
//                                                                       rightMenuViewController:nil];
//        
//        sideMenuViewController.menuPreferredStatusBarStyle = 1; // UIStatusBarStyleLightContent
//        sideMenuViewController.delegate = self;
//        sideMenuViewController.contentViewShadowColor = [UIColor blackColor];
//        sideMenuViewController.contentViewShadowOffset = CGSizeMake(0, 0);
//        sideMenuViewController.contentViewShadowOpacity = 0.6;
//        sideMenuViewController.contentViewShadowRadius = 12;
//        sideMenuViewController.contentViewShadowEnabled = YES;
//        
//        Appdelegate.window.rootViewController = sideMenuViewController;
//        
//        NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
//        [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
//        [Appdelegate deleteDatabase];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Logout" message:@"Are Your Sure?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        [alert show];

    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    

    return [arraymenu count];
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    chatRoomCell *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"chatRoomCell"];
    
    if (cell == nil){
        UIViewController *TempCellView = [[UIViewController alloc] initWithNibName:@"chatRoomCell" bundle:nil];
        cell = (chatRoomCell *)TempCellView.view;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    
    
    cell.accessoryType=UITableViewCellAccessoryNone;
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    [cell setBackgroundColor:[UIColor clearColor]];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.lblName.text=[[arraymenu objectAtIndex:indexPath.row] valueForKey:@"name"];
    [cell.lblName setTextColor:[UIColor whiteColor]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;


    cell.imgUserPic.image=[UIImage imageNamed:[[arraymenu objectAtIndex:indexPath.row] valueForKey:@"img"]];
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath NS_AVAILABLE_IOS(7_0);
{

    return UITableViewAutomaticDimension;


}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return UITableViewAutomaticDimension;
    
    
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section;   // custom view for header. will be adjusted to default or specified header height
{


    return viewheader;

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section;
{

    return  viewheader.frame.size.height;

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    
    if (indexPath.row==0)
    {
        
        
        
        RadioVC *objrd=[Appdelegate.storyboard instantiateViewControllerWithIdentifier:@"RadioVC"];
        
        
        UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:objrd];
        
        nav.navigationBarHidden=true;
        
        ALTabBarController *objal=(ALTabBarController*)self.sideMenuViewController.contentViewController;
        UINavigationController *navbar=(UINavigationController*)[objal selectedViewController];
        UIViewController *obj= [[navbar childViewControllers]lastObject];
        
        
        
            [obj.navigationController  popToRootViewControllerAnimated:YES];
    
        
        [Appdelegate.objapptabbarcontroller tabWasSelected:1];
        [self.sideMenuViewController hideMenuViewController];

    }
    else if(indexPath.row==21)
    {
        
        
        LiveTVVC *objweb=[Appdelegate.storyboard instantiateViewControllerWithIdentifier:@"LiveTVVC"];
     
        
        
        UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:objweb];
        
        nav.navigationBarHidden=true;
        
        ALTabBarController *objal=(ALTabBarController*)self.sideMenuViewController.contentViewController;
        UINavigationController *navbar=(UINavigationController*)[objal selectedViewController];
        UIViewController *obj= [[navbar childViewControllers]lastObject];
        
        [obj.navigationController  popToRootViewControllerAnimated:YES];

        //[obj.navigationController pushViewController:objweb animated:YES];
        
        
        //        [self.sideMenuViewController setContentViewController:objweb
        //                                                     animated:YES];
        
        // [self.sideMenuViewController dismissViewControllerAnimated:YES completion:nil];
        [self.sideMenuViewController hideMenuViewController];
        [Appdelegate.objapptabbarcontroller tabWasSelected:2];

        
        
        
    }
    else if(indexPath.row==22)
    {
    
    
        itunespreviewcnt *objweb=[[itunespreviewcnt alloc]initWithNibName:@"itunespreviewcnt" bundle:nil];
        
        
        
        UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:objweb];
        
        nav.navigationBarHidden=true;
        
        ALTabBarController *objal=(ALTabBarController*)self.sideMenuViewController.contentViewController;
        UINavigationController *navbar=(UINavigationController*)[objal selectedViewController];
        UIViewController *obj= [[navbar childViewControllers]lastObject];
        
     
        [obj.navigationController  popToRootViewControllerAnimated:YES];
        

//        [self.sideMenuViewController setContentViewController:objweb
//                                                     animated:YES];
        [Appdelegate.objapptabbarcontroller tabWasSelected:3];

       // [self.sideMenuViewController dismissViewControllerAnimated:YES completion:nil];
        [self.sideMenuViewController hideMenuViewController];

        
    
    }
    
    else if(indexPath.row==1)
    {
        
        
        
        
        privatebrowser *objweb=[Appdelegate.storyboard instantiateViewControllerWithIdentifier:@"privatebrowser"];
        
        objweb.strweblink=[NSURL URLWithString:@"http://www.catholichub.tv/prayer-request/"];
        
        objweb.strttitle=@"Join Us";
        
        
        UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:objweb];
        
        nav.navigationBarHidden=true;
        
        ALTabBarController *objal=(ALTabBarController*)self.sideMenuViewController.contentViewController;
        UINavigationController *navbar=(UINavigationController*)[objal selectedViewController];
        UIViewController *obj= [[navbar childViewControllers]lastObject];
        
        
        [obj.navigationController pushViewController:objweb animated:YES];
        
        
        //        [self.sideMenuViewController setContentViewController:objweb
        //                                                     animated:YES];
        
        // [self.sideMenuViewController dismissViewControllerAnimated:YES completion:nil];
        [self.sideMenuViewController hideMenuViewController];
        
        
        
        
        
        
    }
    else if(indexPath.row==2)
    {
        
        
        
        
            privatebrowser *objweb=[Appdelegate.storyboard instantiateViewControllerWithIdentifier:@"privatebrowser"];
            
            objweb.strweblink=[NSURL URLWithString:@"http://member.catholichub.org/register.php"];
        
        objweb.strttitle=@"Join Us";

        
            UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:objweb];
            
            nav.navigationBarHidden=true;
            
            ALTabBarController *objal=(ALTabBarController*)self.sideMenuViewController.contentViewController;
            UINavigationController *navbar=(UINavigationController*)[objal selectedViewController];
            UIViewController *obj= [[navbar childViewControllers]lastObject];
            
        
                [obj.navigationController pushViewController:objweb animated:YES];
            
            
            //        [self.sideMenuViewController setContentViewController:objweb
            //                                                     animated:YES];
            
            // [self.sideMenuViewController dismissViewControllerAnimated:YES completion:nil];
            [self.sideMenuViewController hideMenuViewController];
            
            
            

        
        
    }
    else if(indexPath.row==3)
    {
        
        
        TimelinefeedController *objnoti=[Appdelegate.storynewsffed instantiateViewControllerWithIdentifier:@"TimelinefeedController"];
        
        UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:objnoti];
        
        nav.navigationBarHidden=true;
        ALTabBarController *objal=(ALTabBarController*)self.sideMenuViewController.contentViewController;
        UINavigationController *navbar=(UINavigationController*)[objal selectedViewController];
        UIViewController *obj= [[navbar childViewControllers]lastObject];
        
        
        [obj.navigationController pushViewController:objnoti animated:YES];
        [self.sideMenuViewController hideMenuViewController];
        
        
    }
    else if(indexPath.row==4)
    {
        
        
        notificationlist *objnoti=[Appdelegate.storyboard instantiateViewControllerWithIdentifier:@"notificationlist"];
        
        UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:objnoti];
        
        nav.navigationBarHidden=true;
        ALTabBarController *objal=(ALTabBarController*)self.sideMenuViewController.contentViewController;
        UINavigationController *navbar=(UINavigationController*)[objal selectedViewController];
        UIViewController *obj= [[navbar childViewControllers]lastObject];
        
     
            [obj.navigationController pushViewController:objnoti animated:YES];
        [self.sideMenuViewController hideMenuViewController];
        
        
    }
    else if(indexPath.row==7)
    {
    
    
        
        PaymentVC *objvc=[Appdelegate.storyboard instantiateViewControllerWithIdentifier:@"PaymentVC"];
        
        UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:objvc];
        
        nav.navigationBarHidden=true;
        ALTabBarController *objal=(ALTabBarController*)self.sideMenuViewController.contentViewController;
        UINavigationController *navbar=(UINavigationController*)[objal selectedViewController];
        UIViewController *obj= [[navbar childViewControllers]lastObject];
        
  
            [obj.navigationController pushViewController:objvc animated:YES];
        [self.sideMenuViewController hideMenuViewController];
    
    
    
    }
    else if(indexPath.row==6)
    {
        
        
        EventDetails *objevent=[Appdelegate.storyevent instantiateViewControllerWithIdentifier:@"EventDetails"];
        
        UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:objevent];
        
        nav.navigationBarHidden=true;
        ALTabBarController *objal=(ALTabBarController*)self.sideMenuViewController.contentViewController;
        nav.navigationBarHidden=true;
        UINavigationController *navbar=(UINavigationController*)[objal selectedViewController];
        UIViewController *obj= [[navbar childViewControllers]lastObject];
        
        
        [obj.navigationController pushViewController:objevent animated:YES];
        [self.sideMenuViewController hideMenuViewController];

        
        
    }
    else if(indexPath.row==8)
    {
        
        
        ContactUSController *objcont=[Appdelegate.storyboard instantiateViewControllerWithIdentifier:@"ContactUSController"];
        
        UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:objcont];
        
        nav.navigationBarHidden=true;
        ALTabBarController *objal=(ALTabBarController*)self.sideMenuViewController.contentViewController;
        UINavigationController *navbar=(UINavigationController*)[objal selectedViewController];
        UIViewController *obj= [[navbar childViewControllers]lastObject];
        
      
        [obj.navigationController  popToRootViewControllerAnimated:YES];
        
        
        //        [self.sideMenuViewController setContentViewController:objweb
        //                                                     animated:YES];
        [Appdelegate.objapptabbarcontroller tabWasSelected:4];
        
        [self.sideMenuViewController hideMenuViewController];
        
        
    }
    else if(indexPath.row==9)
    {
        
        
  
        
        LoginHomeController *objhome=[Appdelegate.storylogin instantiateViewControllerWithIdentifier:@"LoginHomeController"];
        
        UINavigationController *objnav=[[UINavigationController alloc]initWithRootViewController:objhome];
        objnav.navigationBarHidden=true;
        //[self setUpSideMenu];
        Appdelegate.dicProfiledata=nil;
        [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"profile"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        Appdelegate.window.rootViewController=objnav;
        [Appdelegate.window makeKeyAndVisible];
        
        
    }
    
    
}

//Logout&uid=1
- (IBAction)btnProfileClicked:(id)sender
{
    
    updateProfile *objcont=[Appdelegate.storylogin instantiateViewControllerWithIdentifier:@"updateProfile"];
    
    UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:objcont];
    
    nav.navigationBarHidden=true;
    ALTabBarController *objal=(ALTabBarController*)self.sideMenuViewController.contentViewController;
    UINavigationController *navbar=(UINavigationController*)[objal selectedViewController];
    UIViewController *obj= [[navbar childViewControllers]lastObject];
    
    
    [obj.navigationController pushViewController:objcont animated:YES];
    [self.sideMenuViewController hideMenuViewController];
    
}
@end
