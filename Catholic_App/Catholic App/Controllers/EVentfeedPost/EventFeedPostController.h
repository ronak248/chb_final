//
//  EventFeedPostController.h
//  Catholic HUB
//
//  Created by Ronak on 06/09/15.
//  Copyright (c) 2015 ELITE INFOWORLD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventFeedPostController : UIViewController<UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>
{

    
    IBOutlet UITextField *txtwebsite;
    UIImagePickerController *imagePicker;
    IBOutlet UIButton *btncamerapic;
    UIImage *imgcamera;

}
- (IBAction)btnCameraClicked:(id)sender;
- (IBAction)btnsubmitClicked:(id)sender;
- (IBAction)btnbackClicked:(id)sender;

@end
