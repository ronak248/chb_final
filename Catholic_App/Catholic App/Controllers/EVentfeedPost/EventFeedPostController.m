//
//  EventFeedPostController.m
//  Catholic HUB
//
//  Created by Ronak on 06/09/15.
//  Copyright (c) 2015 ELITE INFOWORLD. All rights reserved.
//

#import "EventFeedPostController.h"
#import <MobileCoreServices/MobileCoreServices.h>

@interface EventFeedPostController ()

@end

@implementation EventFeedPostController

- (void)viewDidLoad {
    
    
    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.mediaTypes =[UIImagePickerController availableMediaTypesForSourceType:imagePicker.sourceType];
    imagePicker.allowsEditing=NO;

    imagePicker.delegate = self;
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma ----------------
#pragma custom button methods
- (IBAction)btnCameraClicked:(id)sender {
    
    UIActionSheet *actionsheetBlockUser=[[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo",@"Photo Library", nil];
    [actionsheetBlockUser showInView:self.view];
}
- (IBAction)btnbackClicked:(id)sender
{

    [self.navigationController popViewControllerAnimated:YES];

}

- (IBAction)btnsubmitClicked:(id)sender {
    
    PFObject *recipe = [PFObject objectWithClassName:@"EventFeed"];
    [recipe setObject:txtwebsite.text forKey:@"eWebsite"];
    
    //2
   
    NSData *imageData = UIImageJPEGRepresentation(imgcamera, 0.8);
    NSString *filename = [NSString stringWithFormat:@"user.png"];
    PFFile *imageFile = [PFFile fileWithName:filename data:imageData];
    [recipe setObject:imageFile forKey:@"Eimage"];

    
    [Appdelegate ShowAnimatedActivityInd];
    
    //3
    [recipe saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [Appdelegate HideAnimatedActivityInd];

        });
        if (error){
            
            NSString *errorString = [[error userInfo] objectForKey:@"error"];
            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:@"Error" message:errorString delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [errorAlertView show];
            
            
            
            //...handle errors
        }
        else
        {
            
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Successfully added event" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            [self.navigationController popViewControllerAnimated:YES];
            
            
            
        }
    }];
    
    
    
}

#pragma---------------------
#pragma mark ActionSheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
   
        if(buttonIndex==0)
        {
            
            if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
            {
                imagePicker.sourceType  = UIImagePickerControllerSourceTypePhotoLibrary;
                [self presentViewController:imagePicker animated:YES completion:nil];
                
            }
            else
            {
                
                imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
                [self presentViewController:imagePicker animated:NO completion:nil];
            }
        }
        else if(buttonIndex==1)
        {
            
            imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:imagePicker animated:NO completion:nil];
            
        }
        
        
    
}

#pragma---------------------
#pragma mark imagepicker Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image=[info valueForKey:UIImagePickerControllerOriginalImage];
    image=[Appdelegate imageWithImage:image scaledToSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.width)];
    [btncamerapic setImage:image forState:UIControlStateNormal];
    imgcamera=image;
    [self dismissViewControllerAnimated:YES completion:nil];
    //        if (isUploadingCoverPic)
    //        {
    //            [self postJsonWithStep:2];
    //        }
    //        else
    //        {
    //}
    
}

@end
