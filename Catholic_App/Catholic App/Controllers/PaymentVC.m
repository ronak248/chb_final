//
//  PaymentVC.m
//  Catholic App
//
//  Created by Ronak on 22/06/15.
//  Copyright (c) 2015 ELITE INFOWORLD. All rights reserved.
//

#import "PaymentVC.h"
#import "privatebrowser.h"
@interface PaymentVC ()

@end

@implementation PaymentVC

- (void)viewDidLoad {
    
    
 

    
    wvPreview.hidden=false;
    rowprograme=1;
    rowpayment=1;
    txtAmount.text=@"$";

    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    gestureRecognizer.delegate=self;
    gestureRecognizer.cancelsTouchesInView = NO;
    [tblView addGestureRecognizer:gestureRecognizer];
    
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(IBAction)btnbackClicked:(id)sender
{

    
    [self.navigationController popViewControllerAnimated:YES];

}

- (IBAction)btnCallclciked:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:+919000499438"]];

}
-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        NSURL *url = request.URL;
        
           // [[UIApplication sharedApplication] openURL:url];

        privatebrowser *objweb=[Appdelegate.storyboard instantiateViewControllerWithIdentifier:@"privatebrowser"];
        
        objweb.strweblink=url;
        
        [self.navigationController pushViewController:objweb animated:YES];
    
    }
    
    return true;
}

-(void)hideKeyboard
{
  
    [txtAmount resignFirstResponder];
[scrView setContentOffset:CGPointMake(0, 0) animated:YES];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    
    NSString *strhtml;
    
   
    
    
}

-(IBAction)btnproceedCLicked:(id)sender
{
    
    
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -tableview delgate methods.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section;   // custom view for footer. will be adjusted to default or specified footer height
{

    if (section==1) {
        return viewheader;
    }
    else if (section==2) {
        return viewheader1;
    }
    return nil;

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{


    return 65;

}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section;
{

  
    if (section==1) {
        return viewheader.frame.size.height;
    }
    else if (section==2) {
        return viewheader1.frame.size.height;
    }
    return 0.0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0) {
        return 1;

    }
    return 0;

    
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section;
{
    
    
    if (section==0) {
        return @"Select Program";
    }
    else if(section==1)
        return @"DIRECT DEPOSIT BANK DETAILS";
    else if(section==2)
        return @"SPONSORSHIP SUPPORT";

    
    return @"";
    
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section;
{
    return 40;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *CellIdentifier = @"CellUser";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    
    
    if (indexPath.section==0) {
        
        if (rowprograme==indexPath.row) {
            
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            
        }
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        
        
        if (indexPath.row==0) {
            
            
            cell.textLabel.text=[NSString stringWithFormat:@"Sponser a TV program Online"];
        }
        else if (indexPath.row==1) {
            
            
            cell.textLabel.text=[NSString stringWithFormat:@"Membership"];
        }
    }
    else if (indexPath.section==1) {
        
        if (rowpayment==indexPath.row) {
            
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            
        }
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
        if (indexPath.row==0) {
            
           

            
            
            cell.textLabel.text=[NSString stringWithFormat:@"Within India"];
        }
        else if (indexPath.row==1) {
            
            
            cell.textLabel.text=[NSString stringWithFormat:@"Outside India"];
        }
    }
  
 
    
    return cell;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    
    
    [scrView setContentOffset:CGPointMake(0, textField.frame.origin.y+100) animated:YES];
    
    
    return TRUE;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    
    
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://catholichub.tv/sponsor-a-tv-program/"]];
    
//    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
//    
//    if (selectedCell.accessoryType==UITableViewCellAccessoryCheckmark) {
//        
//        selectedCell.accessoryType = UITableViewCellAccessoryNone;
// 
//        
//        
//    }
//    else if(selectedCell.accessoryType==UITableViewCellAccessoryNone)
//    {
//        
//        
//        selectedCell.accessoryType = UITableViewCellAccessoryCheckmark;
//        
//        if (indexPath.section==0) {
//            rowprograme=(int)indexPath.row;
//            
//            
//            
//
//        }
//
//        if (indexPath.section==1) {
//            
//            if (indexPath.row==0) {
//                
//                rowselectionfor=11;
//
//                
//                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://catholichub.tv/sponsor-a-tv-program/"]];
//                txtAmount.text=@"₹";
//                
//            }
//            else
//            {
//                
//                rowselectionfor=21;
//
//                txtAmount.text=@"$";
//                
//            }
//
//            rowpayment=(int)indexPath.row;
//
//
//        }
//        
//    }
    
    
    
    
    
    //[tblView reloadData];
    
    
    
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result)
    {
        case MessageComposeResultCancelled:
        {
            //NSLog(@"Cancelled");
            
        }
            
            break;
        case MessageComposeResultFailed:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Fail" message:@"Message Fail to send"
                                                           delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
            break;
            
        case MessageComposeResultSent:
            //NSLog(@"Message Sent Successfully");
            break;
        default:
            break;
    }
    [self dismissViewControllerAnimated:NO completion:nil];
    
}
- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent)
    {
        //NSLog(@"It's away!");
    }
    [self dismissViewControllerAnimated:NO completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnmenuClicked:(id)sender {
    
    [self presentLeftMenuViewController:nil];

}
@end
