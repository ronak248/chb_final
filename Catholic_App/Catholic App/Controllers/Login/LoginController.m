//
//  LoginController.m
//  Catholic HUB
//
//  Created by Ronak on 25/08/15.
//  Copyright (c) 2015 ELITE INFOWORLD. All rights reserved.
//

#import "LoginController.h"
#import "RegistrationController.h"
#import <CoreTelephony/CoreTelephonyDefines.h>
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
@interface LoginController ()

@end

@implementation LoginController

- (void)viewDidLoad {
    
    
    CTTelephonyNetworkInfo *networkInfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    // Get carrier name
    NSString *carrierName = [carrier carrierName];
    if (carrierName != nil)
        NSLog(@"Carrier: %@", carrierName);
    
    // Get mobile country code
    NSString *mcc = [carrier mobileCountryCode];
    if (mcc != nil)
        NSLog(@"Mobile Country Code (MCC): %@", mcc);
    
    
    // Get mobile network code
    NSString *mnc = [carrier mobileNetworkCode];
    if (mnc != nil)
        NSLog(@"Mobile Network Code (MNC): %@", mnc);
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnLoginClicked:(id)sender {
    
    
    
    if ([txtusername.text length]<=0||[txtusername.text isEqualToString:@""]) {
        
        [FVCustomAlertView showDefaultErrorAlertOnView:self.view withTitle:@"Please enter username"];

        
        
    }
    else if ([txtpassword.text length]<=0||[txtpassword.text isEqualToString:@""]) {
        
        [FVCustomAlertView showDefaultErrorAlertOnView:self.view withTitle:@"Please enter password"];
        
        
        
    }
    else
    {
    
    
    
    [Appdelegate ShowAnimatedActivityInd];
    
    PFQuery *query = [PFUser query];

            
            NSString *username = txtusername.text;
            [PFUser logInWithUsernameInBackground:username password:txtpassword.text block:^(PFUser* user, NSError* error){
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [Appdelegate HideAnimatedActivityInd];
                    
                    if (user) {
                        
                        Appdelegate.dicProfiledata=[[NSMutableDictionary alloc]init];
                        
                        [Appdelegate.dicProfiledata setValue:[NSString stringWithFormat:@"%@",[user objectForKey:@"email"]] forKey:@"email"];
                        
                        [Appdelegate.dicProfiledata setValue:[NSString stringWithFormat:@"%@",[user objectForKey:@"fullname"]] forKey:@"fullname"];
                        
                        [Appdelegate.dicProfiledata setValue:[NSString stringWithFormat:@"%@",[user objectForKey:@"username"]] forKey:@"username"];
                        
                        
                        [Appdelegate.dicProfiledata setValue:[NSString stringWithFormat:@"%@",[user objectForKey:@"phone"]] forKey:@"phone"];
                        
                        [[NSUserDefaults standardUserDefaults] setValue:Appdelegate.dicProfiledata forKey:@"profile"];
                        [[NSUserDefaults standardUserDefaults] setValue:user.objectId forKey:@"uid"];

                        [[NSUserDefaults standardUserDefaults] synchronize];
                        [Appdelegate setUpSideMenu];
                        [Appdelegate.window makeKeyAndVisible];
                        //Open the wall
                        
                    } else {
                        
                        //Something bad has ocurred
                        NSString *errorString = [[error userInfo] objectForKey:@"error"];
                        UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:@"Error" message:errorString delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                        [errorAlertView show];
                    }

                   
                });
    
            }];
        
    }
    
    
    
    //    [PFUser logInWithUsernameInBackground:txtemail.text password:txtpass.text block:^(PFUser *user, NSError *error) {
    //        if (user) {
    //            //Open the wall
    //
    //            UINavigationController *objnavigatiocontroller=[[UINavigationController alloc]initWithRootViewController:objProfile];
    //
    //            [self presentViewController:objnavigatiocontroller animated:YES completion:nil];
    //        } else {
    //            //Something bad has ocurred
    //            NSString *errorString = [[error userInfo] objectForKey:@"error"];
    //            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:@"Error" message:errorString delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    //            [errorAlertView show];
    //        }
    //    }];
    

}

- (IBAction)newRegisterClicked:(id)sender {
    
    RegistrationController *objregister=[Appdelegate.storylogin instantiateViewControllerWithIdentifier:@"RegistrationController"];
    [self.navigationController pushViewController:objregister animated:YES];
}

- (IBAction)backClicked:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
@end
