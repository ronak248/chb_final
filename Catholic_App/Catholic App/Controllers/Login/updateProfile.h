//
//  updateProfile.h
//  Catholic HUB
//
//  Created by Ronak on 25/08/15.
//  Copyright (c) 2015 ELITE INFOWORLD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface updateProfile : UIViewController
{

    
    IBOutlet UITextField *txtpassword;
    IBOutlet UIButton *btnreset;
    IBOutlet UIButton *btnupdate;
    IBOutlet UITextField *txtfullname;
    IBOutlet UITextField *txtemail;
    IBOutlet UITextField *txtphone;
}

- (IBAction)updateMethodClicked:(id)sender;

- (IBAction)btnresetButtonClicked:(id)sender;

- (IBAction)btnMenuCLicked:(id)sender;

@end
