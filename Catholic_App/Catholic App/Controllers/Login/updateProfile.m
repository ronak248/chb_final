//
//  updateProfile.m
//  Catholic HUB
//
//  Created by Ronak on 25/08/15.
//  Copyright (c) 2015 ELITE INFOWORLD. All rights reserved.
//

#import "updateProfile.h"
#import "RegistrationController.h"
@interface updateProfile ()

@end

@implementation updateProfile

- (void)viewDidLoad {
    
    
    
    txtfullname.text=[Appdelegate.dicProfiledata valueForKey:@"fullname"];
    txtemail.text=[Appdelegate.dicProfiledata valueForKey:@"email"];
    txtphone.text=[Appdelegate.dicProfiledata valueForKey:@"phone"];


    [[btnupdate layer] setBorderWidth:1.0f];
    [[btnupdate layer] setCornerRadius:1.0f];

    [[btnupdate layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    [[btnreset layer] setCornerRadius:1.0f];

    [[btnreset layer] setBorderWidth:1.0f];
    [[btnreset layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)newRegisterClicked:(id)sender {
    
    RegistrationController *objregister=[Appdelegate.storylogin instantiateViewControllerWithIdentifier:@"RegistrationController"];
    [self.navigationController pushViewController:objregister animated:YES];
}

- (IBAction)backClicked:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (IBAction)updateMethodClicked:(id)sender {
    
    
    
    if ([txtfullname.text length]<=0||[txtfullname.text isEqualToString:@""]) {
        
        [FVCustomAlertView showDefaultErrorAlertOnView:self.view withTitle:@"Please enter fullname"];
        
        
        
    }
    else if ([txtemail.text length]<=0||[txtemail.text isEqualToString:@""]) {
        
        [FVCustomAlertView showDefaultErrorAlertOnView:self.view withTitle:@"Please enter email"];
        
        
        
    }
    else if ([txtphone.text length]<=0||[txtphone.text isEqualToString:@""]) {
        
        [FVCustomAlertView showDefaultErrorAlertOnView:self.view withTitle:@"Please enter phone"];
        
        
        
    }
    else
    {
        
        PFUser *currentUser = [PFUser currentUser];
        NSString *userID = currentUser.objectId;
        
        
        NSLog(@"Parse User ObjectID: %@",userID);
        
        [currentUser setObject:txtfullname.text forKey:@"fullname"];
        [currentUser setObject:txtemail.text forKey:@"email"];
        [currentUser setObject:txtphone.text forKey:@"phone"];
        [Appdelegate ShowAnimatedActivityInd];
        
        [currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (!error) {
                
                
                [PFUser currentUser].password = txtpassword.text;
                [[PFUser currentUser] saveInBackground];
                
                
                [Appdelegate.dicProfiledata setValue:txtemail.text forKey:@"email"];
                
                [Appdelegate.dicProfiledata setValue:txtfullname.text forKey:@"fullname"];
                
                
                [Appdelegate.dicProfiledata setValue:txtphone.text forKey:@"phone"];
                
                [[NSUserDefaults standardUserDefaults] setValue:Appdelegate.dicProfiledata forKey:@"profile"];
                
                [[NSUserDefaults standardUserDefaults] synchronize];
                [FVCustomAlertView showDefaultDoneAlertOnView:self.view withTitle:@"Success"];

                [Appdelegate HideAnimatedActivityInd];

                // The currentUser saved successfully.
            } else {
                NSString *errorString = [[error userInfo] objectForKey:@"error"];
                UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:@"Error" message:errorString delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [errorAlertView show];
                // There was an error saving the currentUser.
            }
        }];
//        [Appdelegate ShowAnimatedActivityInd];
//        
//        PFQuery *query = [PFUser query];
//        
//        
//        NSString *username = txtusername.text;
//        [PFUser logInWithUsernameInBackground:username password:txtpassword.text block:^(PFUser* user, NSError* error){
//            dispatch_async(dispatch_get_main_queue(), ^{
//                
//                [Appdelegate HideAnimatedActivityInd];
//                
//                if (user) {
//                    
//                    Appdelegate.dicProfiledata=[[NSMutableDictionary alloc]init];
//                    
//                    [Appdelegate.dicProfiledata setValue:[NSString stringWithFormat:@"%@",[user objectForKey:@"email"]] forKey:@"email"];
//                    
//                    [Appdelegate.dicProfiledata setValue:[NSString stringWithFormat:@"%@",[user objectForKey:@"fullname"]] forKey:@"fullname"];
//                    
//                    [Appdelegate.dicProfiledata setValue:[NSString stringWithFormat:@"%@",[user objectForKey:@"username"]] forKey:@"username"];
//                    
//                    [Appdelegate.dicProfiledata setValue:[NSString stringWithFormat:@"%@",[user objectForKey:@"phone"]] forKey:@"phone"];
//                    
//                    [[NSUserDefaults standardUserDefaults] setValue:Appdelegate.dicProfiledata forKey:@"profile"];
//                    
//                    [[NSUserDefaults standardUserDefaults] synchronize];
//                    [Appdelegate setUpSideMenu];
//                    [Appdelegate.window makeKeyAndVisible];
//                    //Open the wall
//                    
//                } else {
//                    
//                    //Something bad has ocurred
//                    NSString *errorString = [[error userInfo] objectForKey:@"error"];
//                    UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:@"Error" message:errorString delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//                    [errorAlertView show];
//                }
//                
//                
//            });
//            
//        }];
        
    }
    
    
    
    //    [PFUser logInWithUsernameInBackground:txtemail.text password:txtpass.text block:^(PFUser *user, NSError *error) {
    //        if (user) {
    //            //Open the wall
    //
    //            UINavigationController *objnavigatiocontroller=[[UINavigationController alloc]initWithRootViewController:objProfile];
    //
    //            [self presentViewController:objnavigatiocontroller animated:YES completion:nil];
    //        } else {
    //            //Something bad has ocurred
    //            NSString *errorString = [[error userInfo] objectForKey:@"error"];
    //            UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:@"Error" message:errorString delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    //            [errorAlertView show];
    //        }
    //    }];
    
    
}

- (IBAction)btnresetButtonClicked:(id)sender {
}

- (IBAction)btnMenuCLicked:(id)sender {
    
    [self presentLeftMenuViewController:nil];

}
@end
