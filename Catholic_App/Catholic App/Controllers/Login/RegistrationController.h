//
//  RegistrationController.h
//  Catholic HUB
//
//  Created by Ronak on 25/08/15.
//  Copyright (c) 2015 ELITE INFOWORLD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NIDropDown.h"

@interface RegistrationController : UIViewController<NIDropDownDelegate>
{

    IBOutlet UITextField *txtfullname;
    IBOutlet UITextField *txtemailID;
    IBOutlet UITextField *txtusername;
    IBOutlet UITextField *txtpassword;
    IBOutlet UITextField *txtphoneno;
    NSString *strcode;
    IBOutlet UILabel *lblerrormsg;
    IBOutlet UIButton *btncountrycode;
    NIDropDown *dropDown;
    
    NSMutableArray *arraycountry;
    IBOutlet UIImageView *imgcross;
    IBOutlet UIImageView *imgusername;

}

- (IBAction)btnregisterClicked:(id)sender;
- (IBAction)backloginClicked:(id)sender;
- (IBAction)backClikced:(id)sender;

- (IBAction)btnCodeClicked:(id)sender;

@end
