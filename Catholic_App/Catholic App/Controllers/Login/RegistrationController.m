//
//  RegistrationController.m
//  Catholic HUB
//
//  Created by Ronak on 25/08/15.
//  Copyright (c) 2015 ELITE INFOWORLD. All rights reserved.
//

#import "RegistrationController.h"
#import "LoginController.h"
#import <CoreTelephony/CoreTelephonyDefines.h>
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>


@interface RegistrationController ()

@end

@implementation RegistrationController

- (void)viewDidLoad {
    
    CTTelephonyNetworkInfo *network_Info = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = network_Info.subscriberCellularProvider;
    
    
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"country" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    

    
    
    arraycountry=[json valueForKey:@"countries"];
    NSLog(@"country code is: %@", carrier.mobileCountryCode);
    strcode=carrier.mobileCountryCode;
    [btncountrycode setTitle:@"+91" forState:UIControlStateNormal];
    
    
    

    
    
    NSLocale *locale = [NSLocale currentLocale];
    NSString *countryCode = [locale objectForKey: NSLocaleCountryCode];
    NSDictionary *dict=[Appdelegate dictCountryCodes];
    NSLog(@"%@",[dict objectForKey:countryCode]);
    [btncountrycode setTitle:@"code" forState:UIControlStateNormal];

    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
#pragma mark dropdown method

- (void) niDropDownDelegateMethod: (NIDropDown *) sender
{
    
    strcode=sender.selectedValue;
    [btncountrycode setTitle:sender.selectedValue forState:UIControlStateNormal];

    [self rel];
    
}
-(void)rel
{
    //    [dropDown release];
    dropDown = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnregisterClicked:(id)sender {
    
    
    if ([txtfullname.text length]<=0||[txtfullname.text isEqualToString:@""]) {
        
        [FVCustomAlertView showDefaultErrorAlertOnView:self.view withTitle:@"Please enter fullname"];
        
        
        
    }
    else if ([txtemailID.text length]<=0||[txtemailID.text isEqualToString:@""]) {
        
        [FVCustomAlertView showDefaultErrorAlertOnView:self.view withTitle:@"Please enter email"];
        
        
        
    }
    else if (![Appdelegate validateEmail:txtemailID.text]) {
        
        [FVCustomAlertView showDefaultErrorAlertOnView:self.view withTitle:@"Please enter proper email"];
        
        
        
    }
    else if ([txtphoneno.text length]<=0||[txtphoneno.text isEqualToString:@""]) {
        
        [FVCustomAlertView showDefaultErrorAlertOnView:self.view withTitle:@"Please enter phone"];
        
        
        
    }
    else if ([txtusername.text length]<=0||[txtusername.text isEqualToString:@""]) {
        
        [FVCustomAlertView showDefaultErrorAlertOnView:self.view withTitle:@"Please enter username"];
        
        
        
    }
    else if ([txtpassword.text length]<=0||[txtpassword.text isEqualToString:@""]) {
        
        [FVCustomAlertView showDefaultErrorAlertOnView:self.view withTitle:@"Please enter password"];
    }
    else
    {
        
        
        NSString *strphone=[NSString stringWithFormat:@"%@%@",strcode,txtphoneno.text];
        
        
        
        
        PFUser *user = [PFUser user];
        
        //2
        user.username = txtusername.text;
        user.password = txtpassword.text;
        user.email=txtemailID.text;
        
        [user setObject:txtusername.text forKey:@"username"];
        [user setObject:txtemailID.text forKey:@"email"];
        [user setObject:txtpassword.text forKey:@"password"];
        [user setObject:txtfullname.text forKey:@"fullname"];
        [user setObject:Appdelegate.deviceToken forKey:@"deviceToken"];
        [user setObject:@"ios" forKey:@"plateform"];
        [user setObject:strphone forKey:@"phone"];
        
        
        [Appdelegate ShowAnimatedActivityInd];
        
        //3
        [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [Appdelegate HideAnimatedActivityInd];
                
                if (succeeded) {
                    
                    
                    
                    
                    Appdelegate.dicProfiledata=[[NSMutableDictionary alloc]init];
                    
                    [Appdelegate.dicProfiledata setValue:[NSString stringWithFormat:@"%@",[user objectForKey:@"email"]] forKey:@"email"];
                    
                    [Appdelegate.dicProfiledata setValue:[NSString stringWithFormat:@"%@",[user objectForKey:@"fullname"]] forKey:@"fullname"];
                    
                    [Appdelegate.dicProfiledata setValue:[NSString stringWithFormat:@"%@",[user objectForKey:@"username"]] forKey:@"username"];
                    
                    [Appdelegate.dicProfiledata setValue:[NSString stringWithFormat:@"%@",[user objectForKey:@"phone"]] forKey:@"phone"];
                    [[NSUserDefaults standardUserDefaults] setValue:user.objectId forKey:@"uid"];

                    [[NSUserDefaults standardUserDefaults] setValue:Appdelegate.dicProfiledata forKey:@"profile"];
                    
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    [Appdelegate HideAnimatedActivityInd];
                    
                }
                else
                    [Appdelegate HideAnimatedActivityInd];
                
                
                if (!error) {
                    
                    [Appdelegate HideAnimatedActivityInd];
                    [Appdelegate setUpSideMenu];
                    [Appdelegate.window makeKeyAndVisible];
                    //[hud show:NO];
                    //The registration was successful, go to the wall
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Successfully Registered" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    
                    
                    
                    
                } else {
                    
                    
                    //Something bad has occurred
                    NSString *errorString = [[error userInfo] objectForKey:@"error"];
                    UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:@"Error" message:errorString delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [errorAlertView show];
                    [Appdelegate HideAnimatedActivityInd];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                    });
                    // [self dismissViewControllerAnimated:YES completion:nil];
                    
                }
                
                
            });
            
 
        }];
        
    }

    
    
}

- (IBAction)backloginClicked:(id)sender {
    
    LoginController *objlogin=[Appdelegate.storylogin instantiateViewControllerWithIdentifier:@"LoginController"];
    [self.navigationController pushViewController:objlogin animated:YES];
}

- (IBAction)backClikced:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnCodeClicked:(id)sender {
    
    [txtemailID resignFirstResponder];
    [txtfullname resignFirstResponder];
    [txtpassword resignFirstResponder];
    [txtphoneno resignFirstResponder];
    [txtusername resignFirstResponder];

    
    if(dropDown == nil)
    {
        UIButton *btnfm=(UIButton*)sender;
        
        CGFloat f = [UIScreen mainScreen].bounds.size.height-btnfm.frame.origin.y-btnfm.frame.size.height;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arraycountry :nil :nil :@"down"];
        dropDown.delegate = self;
    }
    else
    {
        [dropDown hideDropDown:sender];
        [self rel];
    }
 
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{

    
    if (textField==txtusername&&[textField.text length]>0) {
        PFQuery *query = [PFUser query];
        [query whereKey:@"username" equalTo:textField.text];
        
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if (object != nil) {
                NSLog(@"User exist");
                lblerrormsg.text=@"Username already exists";
                lblerrormsg.hidden=false;
                imgusername.image=[UIImage imageNamed:@"cross"];
                imgusername.hidden=FALSE;
            }
            else
            {
                lblerrormsg.hidden=true;
                
                imgusername.image=[UIImage imageNamed:@"suc"];
                imgusername.hidden=FALSE;
                NSLog(@"User don`t exist");
            }
        }];
    }
    else if (textField==txtemailID&&[textField.text length]>0) {
        
        
        BOOL isvalidemail=[Appdelegate validateEmail:textField.text];
        if (!isvalidemail&&[textField.text length]>0) {
            
            imgcross.image=[UIImage imageNamed:@"cross"];
            imgcross.hidden=FALSE;

       
            
        }
        else
        {
            
            
            PFQuery *query = [PFUser query];
            [query whereKey:@"email" equalTo:textField.text];
            
            
            
            [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
                if (object != nil) {
                    NSLog(@"User exist");
                    lblerrormsg.text=@"Email ID already exists";
                    lblerrormsg.hidden=false;
                    imgcross.image=[UIImage imageNamed:@"cross"];
                    imgcross.hidden=FALSE;
                }
                else
                {
                    imgcross.image=[UIImage imageNamed:@"suc"];
                    imgcross.hidden=FALSE;
                    NSLog(@"User don`t exist");
                }
            }];
            
        }
    }
 
    
    return true;
}

@end
