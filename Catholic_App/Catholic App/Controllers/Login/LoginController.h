//
//  LoginController.h
//  Catholic HUB
//
//  Created by Ronak on 25/08/15.
//  Copyright (c) 2015 ELITE INFOWORLD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginController : UIViewController
{

    
    IBOutlet UITextField *txtusername;
    IBOutlet UITextField *txtpassword;

}
- (IBAction)btnLoginClicked:(id)sender;
- (IBAction)newRegisterClicked:(id)sender;
- (IBAction)backClicked:(id)sender;

@end
