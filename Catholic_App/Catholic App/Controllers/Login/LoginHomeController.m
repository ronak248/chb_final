//
//  LoginHomeController.m
//  Catholic HUB
//
//  Created by Ronak on 25/08/15.
//  Copyright (c) 2015 ELITE INFOWORLD. All rights reserved.
//

#import "LoginHomeController.h"
#import "LoginController.h"
#import "RegistrationController.h"
@interface LoginHomeController ()

@end

@implementation LoginHomeController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnLoginClicked:(id)sender {
    
    
    LoginController *objlogin=[Appdelegate.storylogin instantiateViewControllerWithIdentifier:@"LoginController"];
    [self.navigationController pushViewController:objlogin animated:YES];
    
}

- (IBAction)btnRegisterClicked:(id)sender {
    
    
    RegistrationController *objregister=[Appdelegate.storylogin instantiateViewControllerWithIdentifier:@"RegistrationController"];
    [self.navigationController pushViewController:objregister animated:YES];
}
@end
