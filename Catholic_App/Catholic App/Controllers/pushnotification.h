//
//  pushnotification.h
//  Catholic App
//
//  Created by Ronak on 02/07/15.
//  Copyright (c) 2015 ELITE INFOWORLD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface pushnotification : UIViewController<UIGestureRecognizerDelegate,UIAlertViewDelegate,MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate>
{

    IBOutlet UITextField *txtname;
    IBOutlet UITextField *txtemail;
    IBOutlet UITextField *txtphone;
    IBOutlet UITextView *txtdescription;


}
-(IBAction)btnsubmitCLicked:(id)sender;
-(IBAction)btnbackClicked:(id)sender;

@end
