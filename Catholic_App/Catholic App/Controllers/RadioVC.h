//
//  RadioVC.h
//  Catholic App
//
//  Created by Ronak on 19/06/15.
//  Copyright (c) 2015 ELITE INFOWORLD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "TOWebViewController.h"

@class AudioStreamer;


@interface RadioVC : UIViewController<UIGestureRecognizerDelegate,UIAlertViewDelegate,MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate,UINavigationControllerDelegate>
{

    AudioStreamer *streamer;
    IBOutlet UIButton *btnplay;
    IBOutlet UIImageView *imgplay;

    IBOutlet UISlider *slidervoulme;
    IBOutlet UIWebView *wvPreview;

    UIImage *imagefb;
    TOWebViewController* webViewController;

    IBOutlet UIView *viewbg;

}
- (IBAction)btnnotificationClikced:(id)sender;

- (IBAction)btnteluguClicked:(id)sender;

- (IBAction)btnEnglishClicked:(id)sender;
- (IBAction)btntamilClicked:(id)sender;
- (IBAction)btnmenuClicked:(id)sender;

-(IBAction)playClicked:(id)sender;
-(IBAction)sliderchange:(id)sender;
-(IBAction)btnfbshare:(id)sender;
-(IBAction)btnpaysponserClicked:(id)sender;
-(IBAction)btnrequestClicked:(id)sender;
-(IBAction)btnregisterClicked:(id)sender;
-(IBAction)btnrefreshClicked:(id)sender;
- (IBAction)btnbackClicked:(id)sender;





@end
