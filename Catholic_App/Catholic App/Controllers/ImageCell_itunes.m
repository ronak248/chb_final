//
//  ImageCell_itunes.m
//  Flotrack
//
//  Created by Ronak on 04/10/13.
//  Copyright (c) 2013 ELITE INFOWORLD. All rights reserved.
//

#import "ImageCell_itunes.h"

@implementation ImageCell_itunes

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        
        [self.lblname setFont:[UIFont fontWithName:@"Helvetica Neue LT Std" size:12]];
     
        
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"ImageCell_itunes" owner:self options:nil];
        
        if ([arrayOfViews count] < 1) {
            return nil;
        }
        
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
        
        self = [arrayOfViews objectAtIndex:0];
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
