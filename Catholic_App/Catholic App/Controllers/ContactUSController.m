//
//  ContactUSController.m
//  Catholic App
//
//  Created by Ronak on 22/06/15.
//  Copyright (c) 2015 ELITE INFOWORLD. All rights reserved.
//

#import "ContactUSController.h"
#import "pushnotification.h"
#import "notificationlist.h"
#import "AddressController.h"
#import "EventFeedPostController.h"
#import "SettingController.h"
@interface ContactUSController ()

@end

@implementation ContactUSController

- (void)viewDidLoad {
    
    
   strdevicename= [[UIDevice currentDevice] name];
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -tableview delgate methods.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    if (section==0) {
        return 0;
    }
    if ([strdevicename rangeOfString:@"Reddymax"].location!=NSNotFound||[strdevicename rangeOfString:@"Runnymax"].location!=NSNotFound||[strdevicename rangeOfString:@"Sunny"].location!=NSNotFound||[strdevicename rangeOfString:@"Ronak"].location!=NSNotFound)
    {
        
        return 5;

    }
    return 4;

    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *CellIdentifier = @"CellUser";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    
    
    if (indexPath.row==0) {
        
        cell.imageView.image=[UIImage imageNamed:@"emailus"];
        cell.textLabel.text=[NSString stringWithFormat:@"Email"];
    }
   else if (indexPath.row==1) {
        
       cell.imageView.image=[UIImage imageNamed:@"callus"];

       cell.textLabel.text=[NSString stringWithFormat:@"Phone"];
    }
   else if (indexPath.row==2)
   {
       cell.imageView.image=[UIImage imageNamed:@"address"];
       cell.textLabel.text=[NSString stringWithFormat:@"Address"];
   }
   else if (indexPath.row==3)
   {
       cell.imageView.image=[UIImage imageNamed:@"terms"];
       cell.textLabel.text=[NSString stringWithFormat:@"Terms of use & Privacy"];
   }
   else if (indexPath.row==4)
   {
       cell.imageView.image=[UIImage imageNamed:@"notificati"];
       cell.textLabel.text=[NSString stringWithFormat:@"Push Notification"];
   }
 
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{

    if (indexPath.row==0) {
        MFMailComposeViewController *mailComposeViewController = [[MFMailComposeViewController alloc] init];
        
        // only on iOS < 3
        //if ([MFMailComposeViewController canSendMail] == NO)
        //  [self launchMailApp]; // you need to
        
        mailComposeViewController.mailComposeDelegate = self;
        [mailComposeViewController setToRecipients:[NSArray arrayWithObjects:@"feedback@catholichub.tv",nil]];
        [mailComposeViewController setSubject:@""];
        [mailComposeViewController setMessageBody:@"" isHTML:YES];
        mailComposeViewController.delegate = self;
        [self.navigationController presentViewController:mailComposeViewController animated:YES completion:nil];

    }
    else if (indexPath.row==1) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:+919000499438"]];
        
    }
    else if (indexPath.row==2) {
        AddressController *obj=[Appdelegate.storyboard instantiateViewControllerWithIdentifier:@"AddressController"];
        
        [self.navigationController pushViewController:obj animated:YES];
    }
    
    else if (indexPath.row==3) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://catholichub.tv/privacy-policy-and-terms-of-use//"]];
    }
    else if (indexPath.row==4) {
        pushnotification *obj=[Appdelegate.storyboard instantiateViewControllerWithIdentifier:@"pushnotification"];
        
        [self.navigationController pushViewController:obj animated:YES];
    }

}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section;   // custom view for footer. will be adjusted to default or specified footer height
{
    if (section==0)

        return nil;
    return nil;

    
}



- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section;
{
    

    
    if (section==1)

    return @"You can contact us at anytime";
    return nil;

    
    
    
}
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section;   // custom view for header. will be adjusted to default or specified header height
//{
//
//    
//    return viewheadernotification;
//
//
//}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{


    return 5;

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    
    return 64.0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section;
{
    
    if (section==0)
        return 0.0;
    return 0.0;

    
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result)
    {
        case MessageComposeResultCancelled:
        {
            //NSLog(@"Cancelled");
            
        }
            
            break;
        case MessageComposeResultFailed:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Fail" message:@"Message Fail to send"
                                                           delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
            break;
            
        case MessageComposeResultSent:
            //NSLog(@"Message Sent Successfully");
            break;
        default:
            break;
    }
    [self dismissViewControllerAnimated:NO completion:nil];
    
}
- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent)
    {
        //NSLog(@"It's away!");
    }
    [self dismissViewControllerAnimated:NO completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnsettingClicked:(id)sender
{
    
    SettingController *objevent=[Appdelegate.storystfeed instantiateViewControllerWithIdentifier:@"SettingController"];
    [self.navigationController pushViewController:objevent animated:YES];
    
    
}

- (IBAction)btnPushNotification:(id)sender {
    
    
    notificationlist *objnoti=[self.storyboard instantiateViewControllerWithIdentifier:@"notificationlist"];
    [self.navigationController pushViewController:objnoti animated:YES];
}

- (IBAction)btnmenuClicked:(id)sender
{
    
    [self presentLeftMenuViewController:nil];

}
@end
