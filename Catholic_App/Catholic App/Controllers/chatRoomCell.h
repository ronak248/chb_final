//
//  chatRoomCell.h
//  ishto
//
//  Created by Tejas Ardeshna on 1/29/15.
//  Copyright (c) 2015 elite infoworld. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface chatRoomCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgUserPic;
@property (strong, nonatomic) IBOutlet UILabel *lblName;

@end
