//
//  AppDelegate.h
//  Catholic App
//
//  Created by Ronak on 19/06/15.
//  Copyright (c) 2015 ELITE INFOWORLD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"ALTabBarController.h"
#import "SideMenuViewController.h"
#import "RESideMenu.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>
{

    UIStoryboard *storyboard;
    UIActivityIndicatorView *activityIndicator;
    BOOL fullScreenVideoIsPlaying;
    NSString *strstreamlink;
    SideMenuViewController *leftViewController;



}
@property  BOOL fullScreenVideoIsPlaying,ispopupframe;
@property  BOOL isvisiblelanguage;
@property NSMutableDictionary * dicProfiledata;
@property (strong, nonatomic) NSString *strstreamlink,*deviceToken,*strstringlength;

@property (strong, nonatomic          ) UIStoryboard                    * storyboard;
@property (strong, nonatomic          ) UIStoryboard                    * storylogin;
@property (strong, nonatomic          ) UIStoryboard                    * storyevent;
@property (strong, nonatomic          ) UIStoryboard                    * storystfeed;
@property (strong, nonatomic          ) UIStoryboard                    * storynewsffed;

@property (strong, nonatomic          ) NSString* strhostname;
@property (strong, nonatomic) UIWindow *window;
@property (strong,nonatomic           ) ALTabBarController              * objapptabbarcontroller;
-(void)ShowAnimatedActivityInd;
-(void)HideAnimatedActivityInd;
-(void)showAlertWithTitle:(NSString *)title andMessge:(NSString *)message;
-(void)setUpSideMenu;
-(BOOL) validateEmail: (NSString *) email;
-(NSDictionary *)dictCountryCodes;
-(UIImage*)imageWithImage:(UIImage*)image
             scaledToSize:(CGSize)newSize;

-(NSString *)youtubelinkget:(NSString *)strlinkk;
-(NSURL *)detectlink:(NSString *)strlinkk;
-(NSString *)soundlinkget:(NSString *)strlinkk;
-(NSString*)HourCalculation:(NSString*)PostDate;

@end

