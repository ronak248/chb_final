//
//  main.m
//  Catholic App
//
//  Created by Ronak on 19/06/15.
//  Copyright (c) 2015 ELITE INFOWORLD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
