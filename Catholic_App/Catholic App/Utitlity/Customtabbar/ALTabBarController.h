//
//  ALTabBarController.h
//  ALCommon
//
//  Created by Andrew Little on 10-08-17.
//  Copyright (c) 2010 Little Apps - www.myroles.ca. All rights reserved.
//
//  Custom TabBarController that hides the iOS TabBar view and displays a custom
//  UI defined in TabBarView.xib.  By customizing TabBarView.xib, you can
//  create a tab bar that is unique to your application, but still has the tab
//  switching functionality you've come to expect out of UITabBarController.


#import <Foundation/Foundation.h>
#import "ALTabBarView.h"
//@class Postmaincnt;
@interface ALTabBarController : UITabBarController <ALTabBarDelegate>
{
    ALTabBarView *customTabBarView;
    int tab1,tab2,tab3,tab4,tab5;
    int lastindex;
    
 //   Postmaincnt *objcnt;
}

@property (nonatomic, retain) IBOutlet ALTabBarView *customTabBarView;
@property (nonatomic, retain) UIViewController *viewcurrent;


-(void) hideExistingTabBar;

-(void) showAnimation;
@end
