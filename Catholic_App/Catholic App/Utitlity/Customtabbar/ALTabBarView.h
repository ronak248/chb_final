//
//  ALTabBarView.h
//  ALCommon
//
//  Created by Andrew Little on 10-08-17.
//  Copyright (c) 2010 Little Apps - www.myroles.ca. All rights reserved.
//
//  Simple custom TabBar view that is defined in the TabBarView nib and used to 
//  replace the standard iOS TabBar view.  By customizing TabBarView.xib, you can
//  create a tab bar that is unique to your application, but still has the tab
//  switching functionality you've come to expect out of UITabBarController.

#import <UIKit/UIKit.h>
//Delegate methods for responding to TabBar events
@protocol ALTabBarDelegate <NSObject>

//Handle tab bar touch events, sending the index of the selected tab
-(void)tabWasSelected:(NSInteger)index;
-(void)slidedown;
-(void)statusupdate;
-(void)doneupdate;
-(void)cancelupdate;
-(void)newpost;

@end

@interface ALTabBarView : UIView {

    NSObject<ALTabBarDelegate> *delegate;
     UIImageView *btntab1;
     UIImageView *btntab2;
    UIImageView *btntab3;
    UIImageView *btntab4;
    UIImageView *btntab5;

     UIView *viewSlide;
    UIView *viewpost;
    UITextView *txtpost;
    int tagClicked;
    UIButton *selectedButton;
    UIImageView *imgdot;
    
    UILabel *lbltitle;
    
    UIImageView *imgactive;
    
    
    
}
@property (strong, nonatomic) IBOutlet UILabel *lblTimelineNotification;
@property (strong, nonatomic) IBOutlet UILabel *lblConvoNotification;

@property (nonatomic, retain)IBOutlet UIImageView *imgactive;
@property (nonatomic, retain)IBOutlet UIImageView *imgactive1;
@property (nonatomic, retain)IBOutlet UIImageView *imgactive2;
@property (nonatomic, retain)IBOutlet UIImageView *imgactive3;
@property (nonatomic, retain)IBOutlet UIImageView *imgactive4;


@property (nonatomic, retain)IBOutlet UIImageView *imgnotification;



@property (nonatomic, retain)  IBOutlet  UIImageView *imgdot;

@property (nonatomic, retain) IBOutlet UILabel *lbltitle;
@property (nonatomic, retain)  IBOutlet UIImageView *btntab1;
@property (nonatomic, retain)  IBOutlet UIView *viewSlide;
@property (nonatomic, retain)  IBOutlet UIView *viewpost;
@property (nonatomic, retain)  IBOutlet UITextView *txtpost;
@property (nonatomic, retain)  IBOutlet UIImageView *btntab2;
@property (nonatomic, retain)  IBOutlet UIImageView *btntab3;
@property (nonatomic, retain)  IBOutlet UIImageView *btntab4;
@property (nonatomic, retain)  IBOutlet UIImageView *btntab5;

@property (nonatomic, retain)  IBOutlet UILabel *lbltimeline;
@property (nonatomic, retain)  IBOutlet UILabel *lblshowcase;
@property (nonatomic, retain)  IBOutlet UILabel *lblexhibition;
@property (nonatomic, retain)  IBOutlet UILabel *lblprofile;
@property (nonatomic, retain)  IBOutlet UILabel *lblmessage;


@property BOOL isClose;
@property (nonatomic, assign) NSObject<ALTabBarDelegate> *delegate;
@property (nonatomic, retain) UIButton *selectedButton;
 @property (nonatomic, readwrite) int tagClicked;

-(IBAction) touchButton:(UIButton *)sender;
-(IBAction) slidedown:(UIButton *)sender;
-(IBAction)statusupdate:(UIButton *)sender;
-(IBAction)cancelCLicked;
-(IBAction)doneClicked;
-(IBAction)newpostClicked;
@end
