//
//  ALTabBarController.m
//  ALCommon
//
//  Created by Andrew Little on 10-08-17.
//  Copyright (c) 2010 Little Apps - www.myroles.ca. All rights reserved.
//

#import "ALTabBarController.h"
//#import "Postmaincnt.h"
//#import "postStatus.h"
@implementation ALTabBarController

@synthesize customTabBarView;

- (void)dealloc {
    
    [customTabBarView release];
    [super dealloc];
}

//-(void)viewDidLoad
//{
//    customTabBarView.isClose=FALSE;
//
//}
- (void)viewDidLoad
{
    //[super viewWillAppear:animated];
    
   
    
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(goToOne) name:@"goToTabOne" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(goToLastTab) name:@"goToLastTab" object:nil];
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(shownotification) name:@"snoti" object:nil];

    
    tab1=0;
    tab2=0;
    tab3=0;
    tab4=0;
    tab5=0;
    lastindex=1;

    
    [self hideExistingTabBar];
    
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removetab) name:@"removetab" object:nil];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addtab) name:@"addtab" object:nil];
    
    
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(slidedown) name:@"slidedown" object:nil];
    
    
   
    
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"TabBarView" owner:self options:nil];
    



    self.customTabBarView = [nibObjects objectAtIndex:0];
    self.customTabBarView.delegate = self;
    
    customTabBarView.lblConvoNotification.frame=CGRectMake(customTabBarView.lblConvoNotification.frame.origin.x, customTabBarView.lblConvoNotification.frame.origin.y, 24 , 24);
    [customTabBarView.lblConvoNotification.layer setCornerRadius:customTabBarView.lblConvoNotification.frame.size.width/2];
    customTabBarView.lblConvoNotification.layer.masksToBounds=true;
    
    [customTabBarView.lblTimelineNotification.layer setCornerRadius:customTabBarView.lblTimelineNotification.frame.size.height/2];
    customTabBarView.lblTimelineNotification.layer.masksToBounds=true;
    customTabBarView.lblTimelineNotification.hidden=true;
    if ([[NSUserDefaults standardUserDefaults]integerForKey:@"convoCount"]>1)
    {
        customTabBarView.lblConvoNotification.hidden=false;
        customTabBarView.lblConvoNotification.text=[NSString stringWithFormat:@"%ld",(long)[[NSUserDefaults standardUserDefaults]integerForKey:@"convoCount"]];
    }
    else
    {
        customTabBarView.lblConvoNotification.hidden=true;
    }
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateNotification) name:@"updateNotification" object:nil];
    
    
    
    
    
    
    
    [customTabBarView.lbltitle setFont:[UIFont fontWithName:@"Helvetica Neue LT Std" size:17]];
    [customTabBarView.txtpost setFont:[UIFont fontWithName:@"Helvetica Neue LT Std" size:14]];
    [customTabBarView.btntab1 setImage:[UIImage imageNamed:@"radio"]  ];
    [customTabBarView.btntab2 setImage:[UIImage imageNamed:@"livetv_sel"]  ];
    [customTabBarView.btntab3 setImage:[UIImage imageNamed:@"video_sel"]  ];
    [customTabBarView.btntab4 setImage:[UIImage imageNamed:@"contact_Sel"]  ];

   // [customTabBarView.btntab2 setImage:[UIImage imageNamed:@"live tv white"]  ];
   // [customTabBarView.btntab3 setImage:[UIImage imageNamed:@"latest video white"]  ];
   // [customTabBarView.btntab4 setImage:[UIImage imageNamed:@"contact white"]];
    
    customTabBarView.imgactive.hidden=false;
    customTabBarView.imgactive1.hidden=true;
    customTabBarView.imgactive2.hidden=true;
    customTabBarView.imgactive3.hidden=true;
    customTabBarView.imgactive4.hidden=true;

    
    
//    [customTabBarView.lbltimeline setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:10]];
//    [customTabBarView.lblshowcase setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:10]];
//    [customTabBarView.lblexhibition setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:10]];
//    [customTabBarView.lblprofile setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:10]];
//    [customTabBarView.lblmessage setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:10]];

   // objhscreen=[[HomeScreenController alloc]initWithNibName:@"HomeScreenController" bundle:nil];
    
    
  
    
    self.customTabBarView.frame = CGRectMake(0, self.view.frame.size.height-50, [UIScreen mainScreen].bounds.size.width, 50);
    [self.view addSubview:self.customTabBarView];
    
    //////NSLog(@"tabselected::%d",self.selectedIndex);
    customTabBarView.tagClicked=self.selectedIndex;
  
}

-(void)updateNotification
{
    if ([[NSUserDefaults standardUserDefaults]integerForKey:@"convoCount"]>0)
    {
        customTabBarView.lblConvoNotification.hidden=false;
        customTabBarView.lblConvoNotification.text=[NSString stringWithFormat:@"%ld",(long)[[NSUserDefaults standardUserDefaults]integerForKey:@"convoCount"]];
    }
    else
    {
        customTabBarView.lblConvoNotification.hidden=true;
    }
}


- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController

{

    return TRUE;
}

-(void)removetab
{

    
    CGRect frame=CGRectMake(0, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width, 50);
    
    if (customTabBarView.frame.origin.y==frame.origin.y) {
        
        
    }

    else
    {
    customTabBarView.frame =CGRectMake(0, [UIScreen mainScreen].bounds.size.height-50, [UIScreen mainScreen].bounds.size.width, 50) ;// somewhere offscreen, in the direction you want it to appear from
    [UIView animateWithDuration:0.5
                     animations:^{
                         customTabBarView.frame =CGRectMake(0, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width, 50) ;// somewhere offscreen, in the direction you want it to appear from
                         
                         
                         
                        
                     }];
        
    }

}

-(void)addtab
{
    
   // [self.view addSubview:objpopup.view];
    //[self.view bringSubviewToFront:objpopup.view];
    //[self.view setBackgroundColor:[UIColor clearColor]];
    
    CGRect frame=CGRectMake(0, [UIScreen mainScreen].bounds.size.height-50, [UIScreen mainScreen].bounds.size.width, 50);
    
    if (customTabBarView.frame.origin.y==frame.origin.y) {
        
        
    }
    else
    {
    
    customTabBarView.frame =CGRectMake(0, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width, 50) ;// somewhere offscreen, in the direction you want it to appear from
    [UIView animateWithDuration:0.5
                     animations:^{
                         customTabBarView.frame =CGRectMake(0, [UIScreen mainScreen].bounds.size.height-50, [UIScreen mainScreen].bounds.size.width, 50) ;// somewhere offscreen, in the direction you want it to appear from
                         
                         
                         
                         
                     }];
        
    }
    //customTabBarView.hidden=FALSE;
    
    
}
- (void)hideExistingTabBar
{
	for(UIView *view in self.view.subviews)
	{
		if([view isKindOfClass:[UITabBar class]])
		{
			view.hidden = TRUE;
			break;
		}
	}
}

#pragma mark ALTabBarDelegate

-(void)tabWasSelected:(NSInteger)index {
    
    
    
    
      customTabBarView.hidden=FALSE;
 
    
 
    
    if (index==1)
    {
        
        
        tab1++;
        if (lastindex==1) {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"popparentcontroller" object:nil];
            tab1=0;
            
        }
        [customTabBarView.lbltitle setFont:[UIFont fontWithName:@"Helvetica Neue LT Std" size:17]];
        [customTabBarView.txtpost setFont:[UIFont fontWithName:@"Helvetica Neue LT Std" size:14]];
        
        [customTabBarView.btntab1 setImage:[UIImage imageNamed:@"radio"]  ];
        [customTabBarView.btntab2 setImage:[UIImage imageNamed:@"livetv_sel"]  ];
        [customTabBarView.btntab3 setImage:[UIImage imageNamed:@"video_sel"]  ];
        [customTabBarView.btntab4 setImage:[UIImage imageNamed:@"contact_Sel"]  ];
        
        
        customTabBarView.imgactive.hidden=false;
        customTabBarView.imgactive1.hidden=true;
        customTabBarView.imgactive2.hidden=true;
        customTabBarView.imgactive3.hidden=true;
        customTabBarView.imgactive4.hidden=true;

        
        
    }
   else if (index==2) {
       
       tab2++;
       
       if (lastindex==2) {
           
           [[NSNotificationCenter defaultCenter] postNotificationName:@"popparentcontroller" object:nil];
           tab2=0;
           
       }
       
       [customTabBarView.lbltitle setFont:[UIFont fontWithName:@"Helvetica Neue LT Std" size:17]];
       [customTabBarView.txtpost setFont:[UIFont fontWithName:@"Helvetica Neue LT Std" size:14]];
       
       customTabBarView.imgactive.hidden=true;
       customTabBarView.imgactive1.hidden=false;
       customTabBarView.imgactive2.hidden=true;
       customTabBarView.imgactive3.hidden=true;
       customTabBarView.imgactive4.hidden=true;
       [customTabBarView.btntab1 setImage:[UIImage imageNamed:@"radio_sel"]  ];
       [customTabBarView.btntab2 setImage:[UIImage imageNamed:@"LiveTV"]  ];
       [customTabBarView.btntab3 setImage:[UIImage imageNamed:@"video_sel"]  ];
       [customTabBarView.btntab4 setImage:[UIImage imageNamed:@"contact_Sel"]  ];
       
//       [customTabBarView.btntab1 setImage:[UIImage imageNamed:@"clock"]  ];
//       [customTabBarView.btntab2 setImage:[UIImage imageNamed:@"showcase_active"]  ];
//       [customTabBarView.btntab3 setImage:[UIImage imageNamed:@"exhibition_active"]  ];
//       [customTabBarView.btntab5 setImage:[UIImage imageNamed:@"profile"]  ];
//       [customTabBarView.btntab4 setImage:[UIImage imageNamed:@"messages_tab"]  ];
       
    }
   else if (index==3) {
       
       tab3++;
       
   
       
       [customTabBarView.lbltitle setFont:[UIFont fontWithName:@"Helvetica Neue LT Std" size:17]];
       [customTabBarView.txtpost setFont:[UIFont fontWithName:@"Helvetica Neue LT Std" size:14]];
       customTabBarView.imgactive.hidden=true;
       customTabBarView.imgactive1.hidden=true;
       customTabBarView.imgactive2.hidden=false;
       customTabBarView.imgactive3.hidden=true;
       customTabBarView.imgactive4.hidden=true;
       
       [customTabBarView.btntab1 setImage:[UIImage imageNamed:@"radio_sel"]  ];
       [customTabBarView.btntab2 setImage:[UIImage imageNamed:@"livetv_sel"]  ];
       [customTabBarView.btntab3 setImage:[UIImage imageNamed:@"video"]  ];
       [customTabBarView.btntab4 setImage:[UIImage imageNamed:@"contact_Sel"]  ];
       
//       [customTabBarView.btntab1 setImage:[UIImage imageNamed:@"clock"]  ];
//       [customTabBarView.btntab2 setImage:[UIImage imageNamed:@"showcase"]  ];
//       [customTabBarView.btntab3 setImage:[UIImage imageNamed:@"exhibition_active"]  ];
//       [customTabBarView.btntab5 setImage:[UIImage imageNamed:@"profile"]  ];
//       [customTabBarView.btntab4 setImage:[UIImage imageNamed:@"messages_tab"]  ];
       
       
       
   }
   else if (index==4) {
       
       
       tab4++;
       
       if (lastindex==4) {
           
           [[NSNotificationCenter defaultCenter] postNotificationName:@"popparentcontroller" object:nil];
           tab4=0;
           
       }
       customTabBarView.imgactive.hidden=true;
       customTabBarView.imgactive1.hidden=true;
       customTabBarView.imgactive2.hidden=true;
       customTabBarView.imgactive3.hidden=false;
       customTabBarView.imgactive4.hidden=true;
       
       [customTabBarView.btntab1 setImage:[UIImage imageNamed:@"radio_sel"]  ];
       [customTabBarView.btntab2 setImage:[UIImage imageNamed:@"livetv_sel"]  ];
       [customTabBarView.btntab3 setImage:[UIImage imageNamed:@"video_sel"]  ];
       [customTabBarView.btntab4 setImage:[UIImage imageNamed:@"contact"]  ];
       
       
       customTabBarView.imgnotification.hidden=true;

//       [customTabBarView.btntab1 setImage:[UIImage imageNamed:@"clock"]  ];
//       [customTabBarView.btntab2 setImage:[UIImage imageNamed:@"showcase"]  ];
//       [customTabBarView.btntab3 setImage:[UIImage imageNamed:@"exhibition_active"]  ];
//       [customTabBarView.btntab5 setImage:[UIImage imageNamed:@"profile"]  ];
//       [customTabBarView.btntab4 setImage:[UIImage imageNamed:@"messages_active"]  ];
       [[NSUserDefaults standardUserDefaults]setInteger:0 forKey:@"convoCount"];
       customTabBarView.lblConvoNotification.hidden=true;
       
   }
   else if (index==5) {
       
       tab5++;
       
       if (lastindex==5) {
           
           [[NSNotificationCenter defaultCenter] postNotificationName:@"popparentcontroller" object:nil];
           tab5=0;
           
       }
       [customTabBarView.viewSlide removeFromSuperview];
       [customTabBarView.viewpost removeFromSuperview];
     //  Appdelegate.isBack=TRUE;
       customTabBarView.imgdot.frame=CGRectMake(289, 58, 6, 6);
       
       [customTabBarView.btntab1 setImage:[UIImage imageNamed:@"button-profile"]  ];
       [customTabBarView.btntab2 setImage:[UIImage imageNamed:@"button-search"]  ];
       [customTabBarView.btntab3 setImage:[UIImage imageNamed:@"button-post"]  ];
       [customTabBarView.btntab4 setImage:[UIImage imageNamed:@"button-archive"]  ];
       [customTabBarView.btntab5 setImage:[UIImage imageNamed:@"active-button-map"]];
       
       customTabBarView.imgactive.hidden=true;
       customTabBarView.imgactive1.hidden=true;
       customTabBarView.imgactive2.hidden=true;
       customTabBarView.imgactive3.hidden=true;
       customTabBarView.imgactive4.hidden=false;
//       [customTabBarView.btntab1 setImage:[UIImage imageNamed:@"clock"]  ];
//       [customTabBarView.btntab2 setImage:[UIImage imageNamed:@"showcase"]  ];
//       [customTabBarView.btntab3 setImage:[UIImage imageNamed:@"exhibition_active"]  ];
//       [customTabBarView.btntab5 setImage:[UIImage imageNamed:@"profile_active"]  ];
//       [customTabBarView.btntab4 setImage:[UIImage imageNamed:@"messages_tab"]  ];
   }
 
  
        //[customTabBarView.btntab3 setImage:[UIImage imageNamed:@"exhibition_active"]  ];
    
    
    UINavigationController *navbar=(UINavigationController*)[self selectedViewController];
    UIViewController *obj= [[navbar childViewControllers]lastObject];
    
    [obj.navigationController popToRootViewControllerAnimated:YES];
    
     lastindex = index;
    
    self.selectedIndex = index-1;
    
    
}
-(void)slidedown
{

    

     //customTabBarView.viewSlide.hidden=TRUE;
   
    customTabBarView.viewSlide.frame =CGRectMake(0, [UIScreen mainScreen].bounds.size.height-250, [UIScreen mainScreen].bounds.size.width, 275) ;// somewhere offscreen, in the direction you want it to appear from
//    [UIView animateWithDuration:2
//                     animations:^{
//                         customTabBarView.viewSlide.frame =CGRectMake(0, [UIScreen mainScreen].bounds.size.height, 320, 275) ;// its final location
    
 
//                         
                         [UIView animateWithDuration:0.5
                                          animations:^{
                                          
                                                 customTabBarView.viewSlide.frame =CGRectMake(0, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width, 275) ;
                                          }
                                          completion:^(BOOL finished){
                                          
                                              [customTabBarView.viewSlide removeFromSuperview];}];
                         
                         
                    // }];
    
    
 


}
-(void)statusupdate
{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"stopmusicprofileMain" object:nil];

    customTabBarView.isClose=FALSE;
   // postStatus *objstatus=[[postStatus alloc]initWithNibName:@"postStatus" bundle:nil];
   // objstatus.view.frame=CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height-40 );
   // [self.view addSubview:objstatus.view];
    [self.view bringSubviewToFront:customTabBarView];
    
   // objstatus.view.frame =CGRectMake(0, [UIScreen mainScreen].bounds.size.height-50, 320, [UIScreen mainScreen].bounds.size.height) ;// somewhere offscreen, in the direction you want it to appear from
    [UIView animateWithDuration:0.5
                     animations:^{
              //           objstatus.view.frame=CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height);
                         
                     }];
    

//    customTabBarView.txtpost.layer.borderWidth = 0.5f;
//     customTabBarView.txtpost.layer.borderColor = [[UIColor blackColor] CGColor];
//    [self.view addSubview:customTabBarView.viewpost];
//    
//    [customTabBarView.txtpost becomeFirstResponder];


}
-(void)doneupdate
{
    
    if ([customTabBarView.txtpost.text length]>0) {
        
        NSString *strstatus=customTabBarView.txtpost.text;
        
       // strstatus=[strstatus stringByReplacingOccurrencesOfString:@"#" withString:CONST_STR];
        NSURL *url;
        NSString* urlstr;
       // urlstr= [NSString stringWithFormat:@"%@UpdateStatus&uid=%@&status=%@",BASE_URL,Appdelegate.strUserID,strstatus];
        
        NSString *strfinal;
        strfinal = [urlstr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        url = [NSURL URLWithString:strfinal];
        [self parsojsonleadresult:url];
        
    }
    
  

}
- (void)textViewDidChange:(UITextView *)textView
{
    
}

- (NSString*)stringWithPercentEscape {
    return [(NSString *) CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)[[self mutableCopy] autorelease], NULL, CFSTR("￼=,!$&'()*+;@?\n\"<>#\t :/"),kCFStringEncodingUTF8) autorelease];
}
- (void) parsojsonleadresult:(NSURL *) jsonURL
{
    //////NSLog(@"%@",jsonURL);
    // Set the queue to the background queue. We will run this on the background thread to keep
    // the UI Responsive.
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
    
    
    
    // Run request on background queue (thread).
    dispatch_async(queue, ^{
        NSError *error = nil;
        
        // Request the data and store in a string.
        NSString *json = [NSString stringWithContentsOfURL:jsonURL
                                                  encoding:NSASCIIStringEncoding
                                                     error:&error];
        if (error == nil){
            
            // Convert the String into an NSData object.
            NSData *jsonData = [json dataUsingEncoding:NSASCIIStringEncoding];
            NSDictionary *jsonDict=[[NSDictionary alloc]init];
            // Parse that data object using NSJSONSerialization without options.
            jsonDict = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
            
            
            
            if (error == nil)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[Appdelegate HideAnimatedActivityInd];
                    if ([[jsonDict valueForKey:@"m"] intValue]==0)
                    {
                        
                        
                        NSString *strmessage=[NSString stringWithFormat:@"%@",@"Failed to post status"];
                        
                        
                        
                    //    [FVCustomAlertView showDefaultErrorAlertOnView:self.view withTitle:strmessage];
                        
                    }
                    else
                    {
                        
                        
                        NSString *strmessage=[NSString stringWithFormat:@"%@",@"Post Uploaded Successfully"];
                      //  [FVCustomAlertView showDefaultDoneAlertOnView:self.view withTitle:strmessage];

                        
                        [customTabBarView.viewpost removeFromSuperview];
                        
                        
                        
                        
                    }
                });
            }
            
            // Parsing failed, display error as alert.
            else
            {
                
                //[SVProgressHUD dismiss];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error!" message:@"Uh Oh, Parsing Failed." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
                [alertView show];
            }
        }
        
        // Request Failed, display error as alert.
        else
        {
            
           // [Appdelegate HideAnimatedActivityInd];
            //[SVProgressHUD dismiss];
           
           //  [FVCustomAlertView showDefaultErrorAlertOnView:self.view withTitle:@"Connection attempt failed"];
        }
    });
}
-(void)cancelupdate
{

 [customTabBarView.viewpost removeFromSuperview];

}
-(void)newpost
{
    
    customTabBarView.isClose=FALSE;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"stopmusicSearchMain" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"stopmusicSearch" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"stopmusicDetail" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"stopmusic" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"stopmusicChartMain" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"stopmusicprofileMain" object:nil];

    
   // objcnt=[[Postmaincnt alloc]initWithNibName:@"Postmaincnt" bundle:nil];
   // objcnt.view.frame=CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height);
   // [self.view addSubview:objcnt.view];
    //[self.view bringSubviewToFront:customTabBarView];
    
    
   // objcnt.view.frame =CGRectMake(0, [UIScreen mainScreen].bounds.size.height-50, 320, [UIScreen mainScreen].bounds.size.height) ;// somewhere offscreen, in the direction you want it to appear from
    [UIView animateWithDuration:0.5
                     animations:^{
                //         objcnt.view.frame=CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height);
                         
                     }];
    
    
    
}
-(void) showAnimation{
    
    
    if (![customTabBarView.viewSlide isDescendantOfView:[self view]]) {
        
        //customTabBarView.btntab3.tag=444;
        
        customTabBarView.isClose=TRUE;
        
        [self.view addSubview:customTabBarView.viewSlide];
        [self.view bringSubviewToFront:customTabBarView];
        [self.view setBackgroundColor:[UIColor clearColor]];
        customTabBarView.viewSlide.frame =CGRectMake(0, [UIScreen mainScreen].bounds.size.height-50, [UIScreen mainScreen].bounds.size.width, 216) ;// somewhere offscreen, in the direction you want it to appear from
        [UIView animateWithDuration:0.5
                         animations:^{
                             customTabBarView.viewSlide.frame =CGRectMake(0, [UIScreen mainScreen].bounds.size.height-260, [UIScreen mainScreen].bounds.size.width, 216) ;// its final location
                         }];
        
    }
    else
    {
        
    customTabBarView.isClose=FALSE;
    
        [self slidedown];
    
    }
    
   
}
-(IBAction) touchButton:(UIButton *)sender
{



}
-(void)shownotification
{

    customTabBarView.imgnotification.hidden=false;
    
}




@end
