//
//  ALTabBarView.m
//  ALCommon
//
//  Created by Andrew Little on 10-08-17.
//  Copyright (c) 2010 Little Apps - www.myroles.ca. All rights reserved.
//

#import "ALTabBarView.h"


@implementation ALTabBarView

@synthesize delegate;
@synthesize selectedButton,tagClicked,btntab1,btntab2,btntab3,btntab4,btntab5,viewSlide,txtpost,viewpost,imgdot,lbltitle,isClose,imgactive;

- (void)dealloc {
    
   
    [selectedButton release];
    delegate = nil;
    [super dealloc];
}

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        // Initialization code
    }
    
    
    
    return self;
}
//-(void)updateNotification
//{
//    if ([[NSUserDefaults standardUserDefaults]integerForKey:@"convoCount"]>1)
//    {
//        lblConvoNotification.hidden=false;
//        lblConvoNotification.text=[NSString stringWithFormat:@"%ld",(long)[[NSUserDefaults standardUserDefaults]integerForKey:@"convoCount"]];
//    }
//    else
//    {
//        lblConvoNotification.hidden=true;
//    }
//}
//Let the delegate know that a tab has been touched
-(IBAction) touchButton:(UIButton *)sender
{

    //if( delegate != nil && [delegate respondsToSelector:@selector(tabWasSelected:)]) {
        
        selectedButton = [((UIButton *)sender) retain];
        //////NSLog(@"tag%d",selectedButton.tag);
    
        
        
        if (selectedButton.tag==1) {
            
        }
        else if (selectedButton.tag==2) {
            
         
            
            
        }
        else if (selectedButton.tag==3) {
            
         
            
        }
        else
        {
        
        
          
        
        }
    
    
        
        [delegate tabWasSelected:selectedButton.tag];
    //}
}

-(IBAction) slidedown:(UIButton *)sender
{

[delegate slidedown];
    
}
-(IBAction)statusupdate:(UIButton *)sender
{

[delegate statusupdate];


}
-(IBAction)cancelCLicked
{

    [delegate cancelupdate];
}
-(IBAction)doneClicked
{

    [delegate doneupdate];

}
-(IBAction)newpostClicked
{


[delegate newpost];

}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController

{
    
    
    return TRUE;
    
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
