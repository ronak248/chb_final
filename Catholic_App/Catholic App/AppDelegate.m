//
//  AppDelegate.m
//  Catholic App
//
//  Created by Ronak on 19/06/15.
//  Copyright (c) 2015 ELITE INFOWORLD. All rights reserved.
//

#import "AppDelegate.h"
#import "RadioVC.h"
#import "LiveTVVC.h"
#import "itunespreviewcnt.h"
#import "ContactUSController.h"
#import <Parse/Parse.h>
#import "LoginHomeController.h"
//#import "ALTabBarController.h"
//NSString *const BFTaskMultipleExceptionsException = @"BFMultipleExceptionsException";

@interface AppDelegate ()<RESideMenuDelegate>

@end

@implementation AppDelegate
@synthesize storyboard,fullScreenVideoIsPlaying,strstreamlink;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    Appdelegate.storylogin=[UIStoryboard storyboardWithName:@"Login" bundle:[NSBundle mainBundle]];
    Appdelegate.storyevent=[UIStoryboard storyboardWithName:@"Eventfeed" bundle:[NSBundle mainBundle]];
    Appdelegate.storystfeed=[UIStoryboard storyboardWithName:@"Statusfeed" bundle:[NSBundle mainBundle]];

    Appdelegate.storynewsffed=[UIStoryboard storyboardWithName:@"Timeline" bundle:[NSBundle mainBundle]];

    
    [self refeshCOntroller];
    
    
    Appdelegate.isvisiblelanguage=true;
    
    [Parse setApplicationId:@"MJ8NfZfgHMHVAumw4ON2oHo5u9N9TIGzpSAR979X"
                  clientKey:@"UA4qVH0p6Iu6XFDNfdwItjRQ8nT6mBZZnRQ5NHKU"];
    
    UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                    UIUserNotificationTypeBadge |
                                                    UIUserNotificationTypeSound);
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                             categories:nil];
    [application registerUserNotificationSettings:settings];
    [application registerForRemoteNotifications];
    
    Appdelegate.deviceToken=@"";
    activityIndicator=[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2-25, [UIScreen mainScreen].bounds.size.height/2, 50, 50)];
    activityIndicator.activityIndicatorViewStyle=UIActivityIndicatorViewStyleWhiteLarge;
    activityIndicator.color=[UIColor redColor];
    activityIndicator.alpha=1.0;
    activityIndicator.hidden=TRUE;
    LoginHomeController *objhome=[Appdelegate.storylogin instantiateViewControllerWithIdentifier:@"LoginHomeController"];
    
    UINavigationController *objnav=[[UINavigationController alloc]initWithRootViewController:objhome];
    objnav.navigationBarHidden=true;
    [self.window addSubview:activityIndicator];
    
    NSMutableDictionary *dicdata=[[NSUserDefaults standardUserDefaults]valueForKey:@"profile"];
    
    if (dicdata==nil) {
        self.window.rootViewController=objnav;

    }
    else
    {
        Appdelegate.dicProfiledata=dicdata;
        [self setUpSideMenu];
    }


    
    
    [self.window makeKeyAndVisible];
   // self.window.rootViewController=Appdelegate.objapptabbarcontroller;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"removetab" object:nil];

    //NSLog(@"%@",[UIFont familyNames]);

    return YES;
}
-(void)setUpSideMenu
{
    

    
    
    leftViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController" bundle:nil];
    
    RESideMenu *sideMenuViewController = [[RESideMenu alloc] initWithContentViewController:Appdelegate.objapptabbarcontroller
                                                                    leftMenuViewController:leftViewController
                                                                   rightMenuViewController:nil];
    
    sideMenuViewController.menuPreferredStatusBarStyle = 1; // UIStatusBarStyleLightContent
    sideMenuViewController.delegate = self;
    sideMenuViewController.contentViewShadowColor = [UIColor blackColor];
    sideMenuViewController.contentViewShadowOffset = CGSizeMake(0, 0);
    sideMenuViewController.contentViewShadowOpacity = 0.6;
    sideMenuViewController.contentViewShadowRadius = 12;
    sideMenuViewController.contentViewShadowEnabled = YES;
    
    self.window.rootViewController = sideMenuViewController;
}
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    // Store the deviceToken in the current installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    
    
    NSString* newDeviceToken1 = [[[[deviceToken description]
                                   stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                  stringByReplacingOccurrencesOfString: @">" withString: @""]
                                 stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    Appdelegate.deviceToken=newDeviceToken1;
    currentInstallation.channels = @[ @"global" ];
    [currentInstallation saveInBackground];
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [PFPush handlePush:userInfo];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"snoti" object:nil];

    
    
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    
    if (Appdelegate.fullScreenVideoIsPlaying==true) {
        return UIInterfaceOrientationMaskAll;
    }
    
    return UIInterfaceOrientationMaskPortrait;
    
}
- (void)applicationWillEnterForeground:(UIApplication *)application {
    


    
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}
-(void)showAlertWithTitle:(NSString *)title andMessge:(NSString *)message
{
    
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    
    [alert show];
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    //[FBAppCall handleDidBecomeActive];

    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
#pragma mark - refesh tabbarcontroller -

-(void)refeshCOntroller
{
    
    
    
    RadioVC* objpost = [self.storyboard instantiateViewControllerWithIdentifier:@"RadioVC"];
    
    LiveTVVC* objtv = [self.storyboard instantiateViewControllerWithIdentifier:@"LiveTVVC"];

    itunespreviewcnt *objitunes=[[itunespreviewcnt alloc]initWithNibName:@"itunespreviewcnt" bundle:nil];
    ContactUSController* objcon = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactUSController"];

    
    UINavigationController*  navigationController1=[[UINavigationController alloc]initWithRootViewController:objpost];
    UINavigationController*  navigationController2=[[UINavigationController alloc]initWithRootViewController:objtv];
    
    UINavigationController*  navigationController3=[[UINavigationController alloc]initWithRootViewController:objitunes];
    
    UINavigationController*  navigationController4=[[UINavigationController alloc]initWithRootViewController:objcon];



    
    ALTabBarController *altabbarcontroler=[[ALTabBarController alloc]init];
    
    navigationController1.navigationBarHidden=TRUE;
    navigationController2.navigationBarHidden=TRUE;
    navigationController3.navigationBarHidden=TRUE;
    navigationController4.navigationBarHidden=TRUE;


    [altabbarcontroler addChildViewController:navigationController1];
    [altabbarcontroler addChildViewController:navigationController2];
    [altabbarcontroler addChildViewController:navigationController3];
    [altabbarcontroler addChildViewController:navigationController4];

    
    Appdelegate.objapptabbarcontroller=altabbarcontroler;
    
    
}

//- (BOOL)application:(UIApplication *)application
//            openURL:(NSURL *)url
//  sourceApplication:(NSString *)sourceApplication
//         annotation:(id)annotation
//{
//    return [FBSession.activeSession handleOpenURL:url];
//
//}
-(void)ShowAnimatedActivityInd
{
    
    activityIndicator.hidden = NO;
    
    [activityIndicator startAnimating];
    
    [self.window bringSubviewToFront:activityIndicator];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
}

-(void)HideAnimatedActivityInd
{
    
    activityIndicator.hidden = YES;
    
    [activityIndicator stopAnimating];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
}
-(BOOL) validateEmail: (NSString *) email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL isValid = [emailTest evaluateWithObject:email];
    return isValid;
}
#pragma -------------------
#pragma get country code methods
-(NSDictionary *)dictCountryCodes{
    NSDictionary *dictCodes = [NSDictionary dictionaryWithObjectsAndKeys:
                               @"93", @"AF",@"20",@"EG", @"355", @"AL", @"213", @"DZ", @"1", @"AS",
                               @"376", @"AD", @"244", @"AO", @"1", @"AI", @"1", @"AG",
                               @"54", @"AR", @"374", @"AM", @"297", @"AW", @"61", @"AU",
                               @"43", @"AT", @"994", @"AZ", @"1", @"BS", @"973", @"BH",
                               @"880", @"BD", @"1", @"BB", @"375", @"BY", @"32", @"BE",
                               @"501", @"BZ", @"229", @"BJ", @"1", @"BM", @"975", @"BT",
                               @"387", @"BA", @"267", @"BW", @"55", @"BR", @"246", @"IO",
                               @"359", @"BG", @"226", @"BF", @"257", @"BI", @"855", @"KH",
                               @"237", @"CM", @"1", @"CA", @"238", @"CV", @"345", @"KY",
                               @"236", @"CF", @"235", @"TD", @"56", @"CL", @"86", @"CN",
                               @"61", @"CX", @"57", @"CO", @"269", @"KM", @"242", @"CG",
                               @"682", @"CK", @"506", @"CR", @"385", @"HR", @"53", @"CU",
                               @"537", @"CY", @"420", @"CZ", @"45", @"DK", @"253", @"DJ",
                               @"1", @"DM", @"1", @"DO", @"593", @"EC", @"20", @"EG",
                               @"503", @"SV", @"240", @"GQ", @"291", @"ER", @"372", @"EE",
                               @"251", @"ET", @"298", @"FO", @"679", @"FJ", @"358", @"FI",
                               @"33", @"FR", @"594", @"GF", @"689", @"PF", @"241", @"GA",
                               @"220", @"GM", @"995", @"GE", @"49", @"DE", @"233", @"GH",
                               @"350", @"GI", @"30", @"GR", @"299", @"GL", @"1", @"GD",
                               @"590", @"GP", @"1", @"GU", @"502", @"GT", @"224", @"GN",
                               @"245", @"GW", @"595", @"GY", @"509", @"HT", @"504", @"HN",
                               @"36", @"HU", @"354", @"IS", @"91", @"IN", @"62", @"ID",
                               @"964", @"IQ", @"353", @"IE", @"972", @"IL", @"39", @"IT",
                               @"1", @"JM", @"81", @"JP", @"962", @"JO", @"77", @"KZ",
                               @"254", @"KE", @"686", @"KI", @"965", @"KW", @"996", @"KG",
                               @"371", @"LV", @"961", @"LB", @"266", @"LS", @"231", @"LR",
                               @"423", @"LI", @"370", @"LT", @"352", @"LU", @"261", @"MG",
                               @"265", @"MW", @"60", @"MY", @"960", @"MV", @"223", @"ML",
                               @"356", @"MT", @"692", @"MH", @"596", @"MQ", @"222", @"MR",
                               @"230", @"MU", @"262", @"YT", @"52", @"MX", @"377", @"MC",
                               @"976", @"MN", @"382", @"ME", @"1", @"MS", @"212", @"MA",
                               @"95", @"MM", @"264", @"NA", @"674", @"NR", @"977", @"NP",
                               @"31", @"NL", @"599", @"AN", @"687", @"NC", @"64", @"NZ",
                               @"505", @"NI", @"227", @"NE", @"234", @"NG", @"683", @"NU",
                               @"672", @"NF", @"1", @"MP", @"47", @"NO", @"968", @"OM",
                               @"92", @"PK", @"680", @"PW", @"507", @"PA", @"675", @"PG",
                               @"595", @"PY", @"51", @"PE", @"63", @"PH", @"48", @"PL",
                               @"351", @"PT", @"1", @"PR", @"974", @"QA", @"40", @"RO",
                               @"250", @"RW", @"685", @"WS", @"378", @"SM", @"966", @"SA",
                               @"221", @"SN", @"381", @"RS", @"248", @"SC", @"232", @"SL",
                               @"65", @"SG", @"421", @"SK", @"386", @"SI", @"677", @"SB",
                               @"27", @"ZA", @"500", @"GS", @"34", @"ES", @"94", @"LK",
                               @"249", @"SD", @"597", @"SR", @"268", @"SZ", @"46", @"SE",
                               @"41", @"CH", @"992", @"TJ", @"66", @"TH", @"228", @"TG",
                               @"690", @"TK", @"676", @"TO", @"1", @"TT", @"216", @"TN",
                               @"90", @"TR", @"993", @"TM", @"1", @"TC", @"688", @"TV",
                               @"256", @"UG", @"380", @"UA", @"971", @"AE", @"44", @"GB",
                               @"1", @"US", @"598", @"UY", @"998", @"UZ", @"678", @"VU",
                               @"681", @"WF", @"967", @"YE", @"260", @"ZM", @"263", @"ZW",
                               @"591", @"BO", @"673", @"BN", @"61", @"CC", @"243", @"CD",
                               @"225", @"CI", @"500", @"FK", @"44", @"GG", @"379", @"VA",
                               @"852", @"HK", @"98", @"IR", @"44", @"IM", @"44", @"JE",
                               @"850", @"KP", @"82", @"KR", @"856", @"LA", @"218", @"LY",
                               @"853", @"MO", @"389", @"MK", @"691", @"FM", @"373", @"MD",
                               @"258", @"MZ", @"970", @"PS", @"872", @"PN", @"262", @"RE",
                               @"7", @"RU", @"590", @"BL", @"290", @"SH", @"1", @"KN",
                               @"1", @"LC", @"590", @"MF", @"508", @"PM", @"1", @"VC",
                               @"239", @"ST", @"252", @"SO", @"47", @"SJ", @"963", @"SY",
                               @"886", @"TW", @"255", @"TZ", @"670", @"TL", @"58", @"VE",
                               @"84", @"VN", @"1", @"VG", @"1", @"VI", nil];
    
    return dictCodes;
}
#pragma mark -imageprocess methods.
-(UIImage*)imageWithImage:(UIImage*)image
             scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark -Linkdetection methods.
-(NSString *)youtubelinkget:(NSString *)strlinkk
{
    
    
    
    NSDataDetector* detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:nil];
    //NSArray* matches = [detector matchesInString:strlinkk options:0 range:NSMakeRange(0, [strlinkk length])];
    // //NSLog(@"arrya::%@",[matches description]);
    
    NSString *strvideolink=nil;
    
    if ([strlinkk length] > 0)
    {
        
        
        NSArray *matches = [detector matchesInString:strlinkk
                            
                                             options:0
                            
                                               range:NSMakeRange(0, [strlinkk length])];
        
        for (NSTextCheckingResult *match in matches)
        {
            if ([match resultType] == NSTextCheckingTypeLink)
            {
                
                NSURL *url = [match URL];
                
                NSString *strli=[url absoluteString];
                
                if ([strli rangeOfString:@"youtu"].location!=NSNotFound)
                {
                    
                    strvideolink=strli;
                    break;
                    
                }
                
            }
            
        }
        
        
        if (strvideolink)
        {
            
            NSString *substringForFirstMatch;
            NSError *error = NULL;
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"(?<=v(=|/))([-a-zA-Z0-9_]+)|(?<=youtu.be/)([-a-zA-Z0-9_]+)" options:NSRegularExpressionCaseInsensitive error:&error];
            NSRange rangeOfFirstMatch = [regex rangeOfFirstMatchInString:strvideolink options:0 range:NSMakeRange(0, [strvideolink length])];
            if(!NSEqualRanges(rangeOfFirstMatch, NSMakeRange(NSNotFound, 0)))
            {
                NSString *substringForFirstMatch = [strvideolink substringWithRange:rangeOfFirstMatch];
                
                return substringForFirstMatch;
            }
            
            
            return substringForFirstMatch;
            
            
        }
    }
    return nil;
    
    
    
}
-(NSString *)soundlinkget:(NSString *)strlinkk
{
    
    
    
    NSDataDetector* detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:nil];
    //NSArray* matches = [detector matchesInString:strlinkk options:0 range:NSMakeRange(0, [strlinkk length])];
    // //NSLog(@"arrya::%@",[matches description]);
    
    NSString *strvideolink=nil;
    
    if ([strlinkk length] > 0)
    {
        
        
        NSArray *matches = [detector matchesInString:strlinkk
                            
                                             options:0
                            
                                               range:NSMakeRange(0, [strlinkk length])];
        
        for (NSTextCheckingResult *match in matches)
        {
            if ([match resultType] == NSTextCheckingTypeLink)
            {
                
                NSURL *url = [match URL];
                
                NSString *strli=[url absoluteString];
                
                if ([strli rangeOfString:@"soundcloud.com"].location!=NSNotFound)
                {
                    
                    strvideolink=strli;
                    break;
                    
                }
                
            }
            
        }
        
        
    }
    
    return strvideolink;
    
    
    
}


-(NSURL *)detectlink:(NSString *)strlinkk
{
    
    
    if (strlinkk==nil) {
        strlinkk=@"";
    }
    NSURL *url ;
    NSDataDetector *detect = [[NSDataDetector alloc] initWithTypes:NSTextCheckingTypeLink error:nil];
    NSArray *matches = [detect matchesInString:strlinkk options:0 range:NSMakeRange(0, [strlinkk length])];
    ////NSLog(@"%@", matches);
    for ( NSTextCheckingResult* match in matches )
    {
        url = [match URL];
    }
    
    NSString *path = [url path];
    
    Appdelegate.strhostname=[url host];
    
    
        return url;
    
    
    
}
-(NSString*)HourCalculation:(NSString*)PostDate

{
    
    PostDate=[NSString stringWithFormat:@"%@",PostDate];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd hh:mm:ss +0000"];
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormat setTimeZone:gmt];
    NSDate *ExpDate = [dateFormat dateFromString:PostDate];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSDayCalendarUnit|NSWeekCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit|NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate:ExpDate toDate:[NSDate date] options:0];
    NSString *time;
    if(components.year!=0)
    {
        if(components.year==1)
        {
            time=[NSString stringWithFormat:@"%ld year",(long)components.year];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld years",(long)components.year];
        }
    }
    else if(components.month!=0)
    {
        if(components.month==1)
        {
            time=[NSString stringWithFormat:@"%ld month",(long)components.month];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld months",(long)components.month];
        }
    }
    else if(components.week!=0)
    {
        if(components.week==1)
        {
            time=[NSString stringWithFormat:@"%ld week",(long)components.week];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld weeks",(long)components.week];
        }
    }
    else if(components.day!=0)
    {
        if(components.day==1)
        {
            time=[NSString stringWithFormat:@"%ld day",(long)components.day];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld days",(long)components.day];
        }
    }
    else if(components.hour!=0)
    {
        if(components.hour==1)
        {
            time=[NSString stringWithFormat:@"%ld hour",(long)components.hour];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld hours",(long)components.hour];
        }
    }
    else if(components.minute!=0)
    {
        if(components.minute==1)
        {
            time=[NSString stringWithFormat:@"%ld min",(long)components.minute];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld mins",(long)components.minute];
        }
    }
    else if(components.second>=0)
    {
        if(components.second==0)
        {
            time=[NSString stringWithFormat:@"1 sec"];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld secs",(long)components.second];
        }
    }
    return [NSString stringWithFormat:@"%@ ago",time];
}
@end
